<?php
/* Copyright (C) 2007-2012  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) 2014       Juanjo Menent       <jmenent@2byte.es>
 * Copyright (C) 2015       Florian Henry       <florian.henry@open-concept.pro>
 * Copyright (C) 2015       Raphaël Doursenaud  <rdoursenaud@gpcsolutions.fr>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    repartos/repartosconductores.class.php
 * \ingroup repartos
 * \brief   This file is an example for a CRUD class file (Create/Read/Update/Delete)
 *          Put some comments here
 */

// Put here all includes required by your class file
require_once DOL_DOCUMENT_ROOT . '/core/class/commonobject.class.php';
//require_once DOL_DOCUMENT_ROOT . '/societe/class/societe.class.php';
//require_once DOL_DOCUMENT_ROOT . '/product/class/product.class.php';

/**
 * Class Repartosconductores
 *
 * Put here description of your class
 * @see CommonObject
 */
class Repartosconductores extends CommonObject
{
	/**
	 * @var string Id to identify managed objects
	 */
	public $element = 'repartosconductores';
	/**
	 * @var string Name of table without prefix where object is stored
	 */
	public $table_element = 'repartos_conductores';

	/**
	 * @var RepartosconductoresLine[] Lines
	 */
	public $lines = array();

	/**
	 */
	
	public $entity;
	public $status;
	public $idconductor;
	public $nombre;
	public $direccion;
	public $telefono;
	public $email;
	public $depto;
	public $puesto;
	public $ingreso = '';
	public $vlicencia = '';
	public $cumpleanios = '';

	/**
	 */
	

	/**
	 * Constructor
	 *
	 * @param DoliDb $db Database handler
	 */
	public function __construct(DoliDB $db)
	{
		$this->db = $db;
		return 1;
	}

	/**
	 * Create object into database
	 *
	 * @param  User $user      User that creates
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 *
	 * @return int <0 if KO, Id of created object if OK
	 */
	public function create(User $user, $notrigger = false)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$error = 0;

		// Clean parameters
		
		if (isset($this->entity)) {
			 $this->entity = trim($this->entity);
		}
		if (isset($this->status)) {
			 $this->status = trim($this->status);
		}
		if (isset($this->idconductor)) {
			 $this->idconductor = trim($this->idconductor);
		}
		if (isset($this->nombre)) {
			 $this->nombre = trim($this->nombre);
		}
		if (isset($this->direccion)) {
			 $this->direccion = trim($this->direccion);
		}
		if (isset($this->telefono)) {
			 $this->telefono = trim($this->telefono);
		}
		if (isset($this->email)) {
			 $this->email = trim($this->email);
		}
		if (isset($this->depto)) {
			 $this->depto = trim($this->depto);
		}
		if (isset($this->puesto)) {
			 $this->puesto = trim($this->puesto);
		}

		

		// Check parameters
		// Put here code to add control on parameters values

		// Insert request
		$sql = 'INSERT INTO ' . MAIN_DB_PREFIX . $this->table_element . '(';
		
		$sql.= 'entity,';
		$sql.= 'status,';
		$sql.= 'idconductor,';
		$sql.= 'nombre,';
		$sql.= 'direccion,';
		$sql.= 'telefono,';
		$sql.= 'email,';
		$sql.= 'depto,';
		$sql.= 'puesto,';
		$sql.= 'ingreso,';
		$sql.= 'vlicencia,';
		$sql.= 'cumpleanios';

		
		$sql .= ') VALUES (';
		
		$sql .= ' '.(! isset($this->entity)?'NULL':$this->entity).',';
		$sql .= ' '.(! isset($this->status)?'NULL':$this->status).',';
		$sql .= ' '.(! isset($this->idconductor)?'NULL':"'".$this->db->escape($this->idconductor)."'").',';
		$sql .= ' '.(! isset($this->nombre)?'NULL':"'".$this->db->escape($this->nombre)."'").',';
		$sql .= ' '.(! isset($this->direccion)?'NULL':"'".$this->db->escape($this->direccion)."'").',';
		$sql .= ' '.(! isset($this->telefono)?'NULL':"'".$this->db->escape($this->telefono)."'").',';
		$sql .= ' '.(! isset($this->email)?'NULL':"'".$this->db->escape($this->email)."'").',';
		$sql .= ' '.(! isset($this->depto)?'NULL':"'".$this->db->escape($this->depto)."'").',';
		$sql .= ' '.(! isset($this->puesto)?'NULL':"'".$this->db->escape($this->puesto)."'").',';
		$sql .= ' '.(! isset($this->ingreso) || dol_strlen($this->ingreso)==0?'NULL':"'".$this->db->idate($this->ingreso)."'").',';
		$sql .= ' '.(! isset($this->vlicencia) || dol_strlen($this->vlicencia)==0?'NULL':"'".$this->db->idate($this->vlicencia)."'").',';
		$sql .= ' '.(! isset($this->cumpleanios) || dol_strlen($this->cumpleanios)==0?'NULL':"'".$this->db->idate($this->cumpleanios)."'");

		
		$sql .= ')';

		$this->db->begin();

		$resql = $this->db->query($sql);
		if (!$resql) {
			$error ++;
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
		}

		if (!$error) {
			$this->id = $this->db->last_insert_id(MAIN_DB_PREFIX . $this->table_element);

			if (!$notrigger) {
				// Uncomment this and change MYOBJECT to your own tag if you
				// want this action to call a trigger.

				//// Call triggers
				//$result=$this->call_trigger('MYOBJECT_CREATE',$user);
				//if ($result < 0) $error++;
				//// End call triggers
			}
		}

		// Commit or rollback
		if ($error) {
			$this->db->rollback();

			return - 1 * $error;
		} else {
			$this->db->commit();

			return $this->id;
		}
	}

	/**
	 * Load object in memory from the database
	 *
	 * @param int    $id  Id object
	 * @param string $ref Ref
	 *
	 * @return int <0 if KO, 0 if not found, >0 if OK
	 */
	public function fetch($id, $ref = null)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$sql = 'SELECT';
		$sql .= ' t.rowid,';
		
		$sql .= " t.entity,";
		$sql .= " t.status,";
		$sql .= " t.idconductor,";
		$sql .= " t.nombre,";
		$sql .= " t.direccion,";
		$sql .= " t.telefono,";
		$sql .= " t.email,";
		$sql .= " t.depto,";
		$sql .= " t.puesto,";
		$sql .= " t.ingreso,";
		$sql .= " t.vlicencia,";
		$sql .= " t.cumpleanios";

		
		$sql .= ' FROM ' . MAIN_DB_PREFIX . $this->table_element . ' as t';
		if (null !== $ref) {
			$sql .= ' WHERE t.ref = ' . '\'' . $ref . '\'';
		} else {
			$sql .= ' WHERE t.rowid = ' . $id;
		}

		$resql = $this->db->query($sql);
		if ($resql) {
			$numrows = $this->db->num_rows($resql);
			if ($numrows) {
				$obj = $this->db->fetch_object($resql);

				$this->id = $obj->rowid;
				
				$this->entity = $obj->entity;
				$this->status = $obj->status;
				$this->idconductor = $obj->idconductor;
				$this->nombre = $obj->nombre;
				$this->direccion = $obj->direccion;
				$this->telefono = $obj->telefono;
				$this->email = $obj->email;
				$this->depto = $obj->depto;
				$this->puesto = $obj->puesto;
				$this->ingreso = $this->db->jdate($obj->ingreso);
				$this->vlicencia = $this->db->jdate($obj->vlicencia);
				$this->cumpleanios = $this->db->jdate($obj->cumpleanios);

				
			}
			$this->db->free($resql);

			if ($numrows) {
				return 1;
			} else {
				return 0;
			}
		} else {
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);

			return - 1;
		}
	}

	/**
	 * Load object in memory from the database
	 *
	 * @param string $sortorder Sort Order
	 * @param string $sortfield Sort field
	 * @param int    $limit     offset limit
	 * @param int    $offset    offset limit
	 * @param array  $filter    filter array
	 * @param string $filtermode filter mode (AND or OR)
	 *
	 * @return int <0 if KO, >0 if OK
	 */
	public function fetchAll($sortorder='', $sortfield='', $limit=0, $offset=0, array $filter = array(), $filtermode='AND')
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$sql = 'SELECT';
		$sql .= ' t.rowid,';
		
		$sql .= " t.entity,";
		$sql .= " t.status,";
		$sql .= " t.idconductor,";
		$sql .= " t.nombre,";
		$sql .= " t.direccion,";
		$sql .= " t.telefono,";
		$sql .= " t.email,";
		$sql .= " t.depto,";
		$sql .= " t.puesto,";
		$sql .= " t.ingreso,";
		$sql .= " t.vlicencia,";
		$sql .= " t.cumpleanios";

		
		$sql .= ' FROM ' . MAIN_DB_PREFIX . $this->table_element. ' as t';

		// Manage filter
		$sqlwhere = array();
		if (count($filter) > 0) {
			foreach ($filter as $key => $value) {
				$sqlwhere [] = $key . ' LIKE \'%' . $this->db->escape($value) . '%\'';
			}
		}
		if (count($sqlwhere) > 0) {
			$sql .= ' WHERE ' . implode(' '.$filtermode.' ', $sqlwhere);
		}
		
		if (!empty($sortfield)) {
			$sql .= $this->db->order($sortfield,$sortorder);
		}
		if (!empty($limit)) {
		 $sql .=  ' ' . $this->db->plimit($limit + 1, $offset);
		}
		$this->lines = array();

		$resql = $this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);

			while ($obj = $this->db->fetch_object($resql)) {
				$line = new RepartosconductoresLine();

				$line->id = $obj->rowid;
				
				$line->entity = $obj->entity;
				$line->status = $obj->status;
				$line->idconductor = $obj->idconductor;
				$line->nombre = $obj->nombre;
				$line->direccion = $obj->direccion;
				$line->telefono = $obj->telefono;
				$line->email = $obj->email;
				$line->depto = $obj->depto;
				$line->puesto = $obj->puesto;
				$line->ingreso = $this->db->jdate($obj->ingreso);
				$line->vlicencia = $this->db->jdate($obj->vlicencia);
				$line->cumpleanios = $this->db->jdate($obj->cumpleanios);

				

				$this->lines[] = $line;
			}
			$this->db->free($resql);

			return $num;
		} else {
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);

			return - 1;
		}
	}

	/**
	 * Update object into database
	 *
	 * @param  User $user      User that modifies
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 *
	 * @return int <0 if KO, >0 if OK
	 */
	public function update(User $user, $notrigger = false)
	{
		$error = 0;

		dol_syslog(__METHOD__, LOG_DEBUG);

		// Clean parameters
		
		if (isset($this->entity)) {
			 $this->entity = trim($this->entity);
		}
		if (isset($this->status)) {
			 $this->status = trim($this->status);
		}
		if (isset($this->idconductor)) {
			 $this->idconductor = trim($this->idconductor);
		}
		if (isset($this->nombre)) {
			 $this->nombre = trim($this->nombre);
		}
		if (isset($this->direccion)) {
			 $this->direccion = trim($this->direccion);
		}
		if (isset($this->telefono)) {
			 $this->telefono = trim($this->telefono);
		}
		if (isset($this->email)) {
			 $this->email = trim($this->email);
		}
		if (isset($this->depto)) {
			 $this->depto = trim($this->depto);
		}
		if (isset($this->puesto)) {
			 $this->puesto = trim($this->puesto);
		}

		

		// Check parameters
		// Put here code to add a control on parameters values

		// Update request
		$sql = 'UPDATE ' . MAIN_DB_PREFIX . $this->table_element . ' SET';
		
		$sql .= ' entity = '.(isset($this->entity)?$this->entity:"null").',';
		$sql .= ' status = '.(isset($this->status)?$this->status:"null").',';
		$sql .= ' idconductor = '.(isset($this->idconductor)?"'".$this->db->escape($this->idconductor)."'":"null").',';
		$sql .= ' nombre = '.(isset($this->nombre)?"'".$this->db->escape($this->nombre)."'":"null").',';
		$sql .= ' direccion = '.(isset($this->direccion)?"'".$this->db->escape($this->direccion)."'":"null").',';
		$sql .= ' telefono = '.(isset($this->telefono)?"'".$this->db->escape($this->telefono)."'":"null").',';
		$sql .= ' email = '.(isset($this->email)?"'".$this->db->escape($this->email)."'":"null").',';
		$sql .= ' depto = '.(isset($this->depto)?"'".$this->db->escape($this->depto)."'":"null").',';
		$sql .= ' puesto = '.(isset($this->puesto)?"'".$this->db->escape($this->puesto)."'":"null").',';
		$sql .= ' ingreso = '.(! isset($this->ingreso) || dol_strlen($this->ingreso) != 0 ? "'".$this->db->idate($this->ingreso)."'" : 'null').',';
		$sql .= ' vlicencia = '.(! isset($this->vlicencia) || dol_strlen($this->vlicencia) != 0 ? "'".$this->db->idate($this->vlicencia)."'" : 'null').',';
		$sql .= ' cumpleanios = '.(! isset($this->cumpleanios) || dol_strlen($this->cumpleanios) != 0 ? "'".$this->db->idate($this->cumpleanios)."'" : 'null');

        
		$sql .= ' WHERE rowid=' . $this->id;

		//echo "sql ".$sql;

		$this->db->begin();

		$resql = $this->db->query($sql);
		if (!$resql) {
			$error ++;
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
		}

		if (!$error && !$notrigger) {
			// Uncomment this and change MYOBJECT to your own tag if you
			// want this action calls a trigger.

			//// Call triggers
			//$result=$this->call_trigger('MYOBJECT_MODIFY',$user);
			//if ($result < 0) { $error++; //Do also what you must do to rollback action if trigger fail}
			//// End call triggers
		}

		// Commit or rollback
		if ($error) {
			$this->db->rollback();

			return - 1 * $error;
		} else {
			$this->db->commit();

			return 1;
		}
	}

	/**
	 * Delete object in database
	 *
	 * @param User $user      User that deletes
	 * @param bool $notrigger false=launch triggers after, true=disable triggers
	 *
	 * @return int <0 if KO, >0 if OK
	 */
	public function delete(User $user, $notrigger = false)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$error = 0;

		$this->db->begin();

		if (!$error) {
			if (!$notrigger) {
				// Uncomment this and change MYOBJECT to your own tag if you
				// want this action calls a trigger.

				//// Call triggers
				//$result=$this->call_trigger('MYOBJECT_DELETE',$user);
				//if ($result < 0) { $error++; //Do also what you must do to rollback action if trigger fail}
				//// End call triggers
			}
		}

		if (!$error) {
			$sql = 'DELETE FROM ' . MAIN_DB_PREFIX . $this->table_element;
			$sql .= ' WHERE rowid=' . $this->id;

			$resql = $this->db->query($sql);
			if (!$resql) {
				$error ++;
				$this->errors[] = 'Error ' . $this->db->lasterror();
				dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
			}
		}

		// Commit or rollback
		if ($error) {
			$this->db->rollback();

			return - 1 * $error;
		} else {
			$this->db->commit();

			return 1;
		}
	}

	/**
	 * Load an object from its id and create a new one in database
	 *
	 * @param int $fromid Id of object to clone
	 *
	 * @return int New id of clone
	 */
	public function createFromClone($fromid)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		global $user;
		$error = 0;
		$object = new Repartosconductores($this->db);

		$this->db->begin();

		// Load source object
		$object->fetch($fromid);
		// Reset object
		$object->id = 0;

		// Clear fields
		// ...

		// Create clone
		$result = $object->create($user);

		// Other options
		if ($result < 0) {
			$error ++;
			$this->errors = $object->errors;
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
		}

		// End
		if (!$error) {
			$this->db->commit();

			return $object->id;
		} else {
			$this->db->rollback();

			return - 1;
		}
	}

	/**
	 *  Return a link to the user card (with optionaly the picto)
	 * 	Use this->id,this->lastname, this->firstname
	 *
	 *	@param	int		$withpicto			Include picto in link (0=No picto, 1=Include picto into link, 2=Only picto)
	 *	@param	string	$option				On what the link point to
     *  @param	integer	$notooltip			1=Disable tooltip
     *  @param	int		$maxlen				Max length of visible user name
     *  @param  string  $morecss            Add more css on link
	 *	@return	string						String with URL
	 */
	function getNomUrl($withpicto=0, $option='', $notooltip=0, $maxlen=24, $morecss='')
	{
		global $langs, $conf, $db;
        global $dolibarr_main_authentication, $dolibarr_main_demo;
        global $menumanager;


        $result = '';
        $companylink = '';

        $label = '<u>' . $langs->trans("rep_titre") . '</u>';
        $label.= '<div width="100%">';
        $label.= '<b>' . $langs->trans('rep_idconduc') . ':</b> ' . $this->idconductor;
        $label.= '<br><b>' . $langs->trans('rep_status') . ':</b> ' . $langs->trans('rep_status'.$this->status);

        $link = '<a href="'.DOL_URL_ROOT.'/repartos/conductor/repartosconductores_card.php?action=view&id='.$this->id.'"';
        $link.= ($notooltip?'':' title="'.dol_escape_htmltag($label, 1).'" class="classfortooltip'.($morecss?' '.$morecss:'').'"');
        $link.= '>';
		$linkend='</a>';

        if ($withpicto)
        {
            $result.=($link.img_object(($notooltip?'':$label), 'contact', ($notooltip?'':'class="classfortooltip"')).$linkend);
            if ($withpicto != 2) $result.=' ';
		}
		$result.= $link . $this->idconductor . $linkend;
		return $result;
	}
	
	/**
	 *  Retourne le libelle du status d'un user (actif, inactif)
	 *
	 *  @param	int		$mode          0=libelle long, 1=libelle court, 2=Picto + Libelle court, 3=Picto, 4=Picto + Libelle long, 5=Libelle court + Picto
	 *  @return	string 			       Label of status
	 */
	function getLibStatut($mode=0)
	{
		return $this->LibStatut($this->status,$mode);
	}

	/**
	 *  Renvoi le libelle d'un status donne
	 *
	 *  @param	int		$status        	Id status
	 *  @param  int		$mode          	0=libelle long, 1=libelle court, 2=Picto + Libelle court, 3=Picto, 4=Picto + Libelle long, 5=Libelle court + Picto
	 *  @return string 			       	Label of status
	 */
	function LibStatut($status,$mode=0)
	{
		global $langs;

		if ($mode == 0)
		{
			$prefix='';
			if ($status == 1) return $langs->trans('Enabled');
			if ($status == 0) return $langs->trans('Disabled');
		}
		if ($mode == 1)
		{
			if ($status == 1) return $langs->trans('Enabled');
			if ($status == 0) return $langs->trans('Disabled');
		}
		if ($mode == 2)
		{
			if ($status == 1) return img_picto($langs->trans('Enabled'),'statut4').' '.$langs->trans('Enabled');
			if ($status == 0) return img_picto($langs->trans('Disabled'),'statut5').' '.$langs->trans('Disabled');
		}
		if ($mode == 3)
		{
			if ($status == 1) return img_picto($langs->trans('Enabled'),'statut4');
			if ($status == 0) return img_picto($langs->trans('Disabled'),'statut5');
		}
		if ($mode == 4)
		{
			if ($status == 1) return img_picto($langs->trans('Enabled'),'statut4').' '.$langs->trans('Enabled');
			if ($status == 0) return img_picto($langs->trans('Disabled'),'statut5').' '.$langs->trans('Disabled');
		}
		if ($mode == 5)
		{
			if ($status == 1) return $langs->trans('Enabled').' '.img_picto($langs->trans('Enabled'),'statut4');
			if ($status == 0) return $langs->trans('Disabled').' '.img_picto($langs->trans('Disabled'),'statut5');
		}
	}
	
	
	/**
	 * Initialise object with example values
	 * Id must be 0 if object instance is a specimen
	 *
	 * @return void
	 */
	public function initAsSpecimen()
	{
		$this->id = 0;
		
		$this->entity = '';
		$this->status = '';
		$this->idconductor = '';
		$this->nombre = '';
		$this->direccion = '';
		$this->telefono = '';
		$this->email = '';
		$this->depto = '';
		$this->puesto = '';
		$this->ingreso = '';
		$this->vlicencia = '';
		$this->cumpleanios = '';

		
	}

}

/**
 * Class RepartosconductoresLine
 */
class RepartosconductoresLine
{
	/**
	 * @var int ID
	 */
	public $id;
	/**
	 * @var mixed Sample line property 1
	 */
	
	public $entity;
	public $status;
	public $idconductor;
	public $nombre;
	public $direccion;
	public $telefono;
	public $email;
	public $depto;
	public $puesto;
	public $ingreso = '';
	public $vlicencia = '';
	public $cumpleanios = '';

	/**
	 * @var mixed Sample line property 2
	 */
	
}
