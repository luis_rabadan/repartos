<?php
/* Copyright (C) 2007-2012  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) 2014       Juanjo Menent       <jmenent@2byte.es>
 * Copyright (C) 2015       Florian Henry       <florian.henry@open-concept.pro>
 * Copyright (C) 2015       Raphaël Doursenaud  <rdoursenaud@gpcsolutions.fr>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    repartos/facture.class.php
 * \ingroup repartos
 * \brief   This file is an example for a CRUD class file (Create/Read/Update/Delete)
 *          Put some comments here
 */

// Put here all includes required by your class file
require_once DOL_DOCUMENT_ROOT . '/core/class/commonobject.class.php';
//require_once DOL_DOCUMENT_ROOT . '/societe/class/societe.class.php';
//require_once DOL_DOCUMENT_ROOT . '/product/class/product.class.php';

/**
 * Class Facture
 *
 * Put here description of your class
 * @see CommonObject
 */
class Facture extends CommonObject
{
	/**
	 * @var string Id to identify managed objects
	 */
	public $element = 'facture';
	/**
	 * @var string Name of table without prefix where object is stored
	 */
	public $table_element = 'facture';

	/**
	 * @var FactureLine[] Lines
	 */
	public $lines = array();

	/**
	 */
	
	public $facnumber;
	public $entity;
	public $ref_ext;
	public $ref_int;
	public $ref_client;
	public $type;
	public $increment;
	public $fk_soc;
	public $datec = '';
	public $datef = '';
	public $date_valid = '';
	public $tms = '';
	public $paye;
	public $amount;
	public $remise_percent;
	public $remise_absolue;
	public $remise;
	public $close_code;
	public $close_note;
	public $tva;
	public $localtax1;
	public $localtax2;
	public $revenuestamp;
	public $total;
	public $total_ttc;
	public $fk_statut;
	public $fk_user_author;
	public $fk_user_modif;
	public $fk_user_valid;
	public $fk_facture_source;
	public $fk_projet;
	public $fk_account;
	public $fk_currency;
	public $fk_cond_reglement;
	public $fk_mode_reglement;
	public $date_lim_reglement = '';
	public $note_private;
	public $note_public;
	public $model_pdf;
	public $fk_incoterms;
	public $location_incoterms;
	public $situation_cycle_ref;
	public $situation_counter;
	public $situation_final;
	public $import_key;
	public $extraparams;

	/**
	 */
	

	/**
	 * Constructor
	 *
	 * @param DoliDb $db Database handler
	 */
	public function __construct(DoliDB $db)
	{
		$this->db = $db;
		return 1;
	}

	/**
	 * Create object into database
	 *
	 * @param  User $user      User that creates
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 *
	 * @return int <0 if KO, Id of created object if OK
	 */
	public function create(User $user, $notrigger = false)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$error = 0;

		// Clean parameters
		
		if (isset($this->facnumber)) {
			 $this->facnumber = trim($this->facnumber);
		}
		if (isset($this->entity)) {
			 $this->entity = trim($this->entity);
		}
		if (isset($this->ref_ext)) {
			 $this->ref_ext = trim($this->ref_ext);
		}
		if (isset($this->ref_int)) {
			 $this->ref_int = trim($this->ref_int);
		}
		if (isset($this->ref_client)) {
			 $this->ref_client = trim($this->ref_client);
		}
		if (isset($this->type)) {
			 $this->type = trim($this->type);
		}
		if (isset($this->increment)) {
			 $this->increment = trim($this->increment);
		}
		if (isset($this->fk_soc)) {
			 $this->fk_soc = trim($this->fk_soc);
		}
		if (isset($this->paye)) {
			 $this->paye = trim($this->paye);
		}
		if (isset($this->amount)) {
			 $this->amount = trim($this->amount);
		}
		if (isset($this->remise_percent)) {
			 $this->remise_percent = trim($this->remise_percent);
		}
		if (isset($this->remise_absolue)) {
			 $this->remise_absolue = trim($this->remise_absolue);
		}
		if (isset($this->remise)) {
			 $this->remise = trim($this->remise);
		}
		if (isset($this->close_code)) {
			 $this->close_code = trim($this->close_code);
		}
		if (isset($this->close_note)) {
			 $this->close_note = trim($this->close_note);
		}
		if (isset($this->tva)) {
			 $this->tva = trim($this->tva);
		}
		if (isset($this->localtax1)) {
			 $this->localtax1 = trim($this->localtax1);
		}
		if (isset($this->localtax2)) {
			 $this->localtax2 = trim($this->localtax2);
		}
		if (isset($this->revenuestamp)) {
			 $this->revenuestamp = trim($this->revenuestamp);
		}
		if (isset($this->total)) {
			 $this->total = trim($this->total);
		}
		if (isset($this->total_ttc)) {
			 $this->total_ttc = trim($this->total_ttc);
		}
		if (isset($this->fk_statut)) {
			 $this->fk_statut = trim($this->fk_statut);
		}
		if (isset($this->fk_user_author)) {
			 $this->fk_user_author = trim($this->fk_user_author);
		}
		if (isset($this->fk_user_modif)) {
			 $this->fk_user_modif = trim($this->fk_user_modif);
		}
		if (isset($this->fk_user_valid)) {
			 $this->fk_user_valid = trim($this->fk_user_valid);
		}
		if (isset($this->fk_facture_source)) {
			 $this->fk_facture_source = trim($this->fk_facture_source);
		}
		if (isset($this->fk_projet)) {
			 $this->fk_projet = trim($this->fk_projet);
		}
		if (isset($this->fk_account)) {
			 $this->fk_account = trim($this->fk_account);
		}
		if (isset($this->fk_currency)) {
			 $this->fk_currency = trim($this->fk_currency);
		}
		if (isset($this->fk_cond_reglement)) {
			 $this->fk_cond_reglement = trim($this->fk_cond_reglement);
		}
		if (isset($this->fk_mode_reglement)) {
			 $this->fk_mode_reglement = trim($this->fk_mode_reglement);
		}
		if (isset($this->note_private)) {
			 $this->note_private = trim($this->note_private);
		}
		if (isset($this->note_public)) {
			 $this->note_public = trim($this->note_public);
		}
		if (isset($this->model_pdf)) {
			 $this->model_pdf = trim($this->model_pdf);
		}
		if (isset($this->fk_incoterms)) {
			 $this->fk_incoterms = trim($this->fk_incoterms);
		}
		if (isset($this->location_incoterms)) {
			 $this->location_incoterms = trim($this->location_incoterms);
		}
		if (isset($this->situation_cycle_ref)) {
			 $this->situation_cycle_ref = trim($this->situation_cycle_ref);
		}
		if (isset($this->situation_counter)) {
			 $this->situation_counter = trim($this->situation_counter);
		}
		if (isset($this->situation_final)) {
			 $this->situation_final = trim($this->situation_final);
		}
		if (isset($this->import_key)) {
			 $this->import_key = trim($this->import_key);
		}
		if (isset($this->extraparams)) {
			 $this->extraparams = trim($this->extraparams);
		}

		

		// Check parameters
		// Put here code to add control on parameters values

		// Insert request
		$sql = 'INSERT INTO ' . MAIN_DB_PREFIX . $this->table_element . '(';
		
		$sql.= 'facnumber,';
		$sql.= 'entity,';
		$sql.= 'ref_ext,';
		$sql.= 'ref_int,';
		$sql.= 'ref_client,';
		$sql.= 'type,';
		$sql.= 'increment,';
		$sql.= 'fk_soc,';
		$sql.= 'datec,';
		$sql.= 'datef,';
		$sql.= 'date_valid,';
		$sql.= 'paye,';
		$sql.= 'amount,';
		$sql.= 'remise_percent,';
		$sql.= 'remise_absolue,';
		$sql.= 'remise,';
		$sql.= 'close_code,';
		$sql.= 'close_note,';
		$sql.= 'tva,';
		$sql.= 'localtax1,';
		$sql.= 'localtax2,';
		$sql.= 'revenuestamp,';
		$sql.= 'total,';
		$sql.= 'total_ttc,';
		$sql.= 'fk_statut,';
		$sql.= 'fk_user_author,';
		$sql.= 'fk_user_modif,';
		$sql.= 'fk_user_valid,';
		$sql.= 'fk_facture_source,';
		$sql.= 'fk_projet,';
		$sql.= 'fk_account,';
		$sql.= 'fk_currency,';
		$sql.= 'fk_cond_reglement,';
		$sql.= 'fk_mode_reglement,';
		$sql.= 'date_lim_reglement,';
		$sql.= 'note_private,';
		$sql.= 'note_public,';
		$sql.= 'model_pdf,';
		$sql.= 'fk_incoterms,';
		$sql.= 'location_incoterms,';
		$sql.= 'situation_cycle_ref,';
		$sql.= 'situation_counter,';
		$sql.= 'situation_final,';
		$sql.= 'import_key';
		$sql.= 'extraparams';

		
		$sql .= ') VALUES (';
		
		$sql .= ' '.(! isset($this->facnumber)?'NULL':"'".$this->db->escape($this->facnumber)."'").',';
		$sql .= ' '.(! isset($this->entity)?'NULL':$this->entity).',';
		$sql .= ' '.(! isset($this->ref_ext)?'NULL':"'".$this->db->escape($this->ref_ext)."'").',';
		$sql .= ' '.(! isset($this->ref_int)?'NULL':"'".$this->db->escape($this->ref_int)."'").',';
		$sql .= ' '.(! isset($this->ref_client)?'NULL':"'".$this->db->escape($this->ref_client)."'").',';
		$sql .= ' '.(! isset($this->type)?'NULL':$this->type).',';
		$sql .= ' '.(! isset($this->increment)?'NULL':"'".$this->db->escape($this->increment)."'").',';
		$sql .= ' '.(! isset($this->fk_soc)?'NULL':$this->fk_soc).',';
		$sql .= ' '."'".$this->db->idate(dol_now())."'".',';
		$sql .= ' '.(! isset($this->datef) || dol_strlen($this->datef)==0?'NULL':"'".$this->db->idate($this->datef)."'").',';
		$sql .= ' '.(! isset($this->date_valid) || dol_strlen($this->date_valid)==0?'NULL':"'".$this->db->idate($this->date_valid)."'").',';
		$sql .= ' '.(! isset($this->paye)?'NULL':$this->paye).',';
		$sql .= ' '.(! isset($this->amount)?'NULL':"'".$this->amount."'").',';
		$sql .= ' '.(! isset($this->remise_percent)?'NULL':"'".$this->remise_percent."'").',';
		$sql .= ' '.(! isset($this->remise_absolue)?'NULL':"'".$this->remise_absolue."'").',';
		$sql .= ' '.(! isset($this->remise)?'NULL':"'".$this->remise."'").',';
		$sql .= ' '.(! isset($this->close_code)?'NULL':"'".$this->db->escape($this->close_code)."'").',';
		$sql .= ' '.(! isset($this->close_note)?'NULL':"'".$this->db->escape($this->close_note)."'").',';
		$sql .= ' '.(! isset($this->tva)?'NULL':"'".$this->tva."'").',';
		$sql .= ' '.(! isset($this->localtax1)?'NULL':"'".$this->localtax1."'").',';
		$sql .= ' '.(! isset($this->localtax2)?'NULL':"'".$this->localtax2."'").',';
		$sql .= ' '.(! isset($this->revenuestamp)?'NULL':"'".$this->revenuestamp."'").',';
		$sql .= ' '.(! isset($this->total)?'NULL':"'".$this->total."'").',';
		$sql .= ' '.(! isset($this->total_ttc)?'NULL':"'".$this->total_ttc."'").',';
		$sql .= ' '.(! isset($this->fk_statut)?'NULL':$this->fk_statut).',';
		$sql .= ' '.$user->id.',';
		$sql .= ' '.(! isset($this->fk_user_modif)?'NULL':$this->fk_user_modif).',';
		$sql .= ' '.(! isset($this->fk_user_valid)?'NULL':$this->fk_user_valid).',';
		$sql .= ' '.(! isset($this->fk_facture_source)?'NULL':$this->fk_facture_source).',';
		$sql .= ' '.(! isset($this->fk_projet)?'NULL':$this->fk_projet).',';
		$sql .= ' '.(! isset($this->fk_account)?'NULL':$this->fk_account).',';
		$sql .= ' '.(! isset($this->fk_currency)?'NULL':"'".$this->db->escape($this->fk_currency)."'").',';
		$sql .= ' '.(! isset($this->fk_cond_reglement)?'NULL':$this->fk_cond_reglement).',';
		$sql .= ' '.(! isset($this->fk_mode_reglement)?'NULL':$this->fk_mode_reglement).',';
		$sql .= ' '.(! isset($this->date_lim_reglement) || dol_strlen($this->date_lim_reglement)==0?'NULL':"'".$this->db->idate($this->date_lim_reglement)."'").',';
		$sql .= ' '.(! isset($this->note_private)?'NULL':"'".$this->db->escape($this->note_private)."'").',';
		$sql .= ' '.(! isset($this->note_public)?'NULL':"'".$this->db->escape($this->note_public)."'").',';
		$sql .= ' '.(! isset($this->model_pdf)?'NULL':"'".$this->db->escape($this->model_pdf)."'").',';
		$sql .= ' '.(! isset($this->fk_incoterms)?'NULL':$this->fk_incoterms).',';
		$sql .= ' '.(! isset($this->location_incoterms)?'NULL':"'".$this->db->escape($this->location_incoterms)."'").',';
		$sql .= ' '.(! isset($this->situation_cycle_ref)?'NULL':$this->situation_cycle_ref).',';
		$sql .= ' '.(! isset($this->situation_counter)?'NULL':$this->situation_counter).',';
		$sql .= ' '.(! isset($this->situation_final)?'NULL':$this->situation_final).',';
		$sql .= ' '.(! isset($this->import_key)?'NULL':"'".$this->db->escape($this->import_key)."'").',';
		$sql .= ' '.(! isset($this->extraparams)?'NULL':"'".$this->db->escape($this->extraparams)."'");

		
		$sql .= ')';

		$this->db->begin();

		$resql = $this->db->query($sql);
		if (!$resql) {
			$error ++;
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
		}

		if (!$error) {
			$this->id = $this->db->last_insert_id(MAIN_DB_PREFIX . $this->table_element);

			if (!$notrigger) {
				// Uncomment this and change MYOBJECT to your own tag if you
				// want this action to call a trigger.

				//// Call triggers
				//$result=$this->call_trigger('MYOBJECT_CREATE',$user);
				//if ($result < 0) $error++;
				//// End call triggers
			}
		}

		// Commit or rollback
		if ($error) {
			$this->db->rollback();

			return - 1 * $error;
		} else {
			$this->db->commit();

			return $this->id;
		}
	}

	/**
	 * Load object in memory from the database
	 *
	 * @param int    $id  Id object
	 * @param string $ref Ref
	 *
	 * @return int <0 if KO, 0 if not found, >0 if OK
	 */
	public function fetch($id, $ref = null)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$sql = 'SELECT';
		$sql .= ' t.rowid,';
		
		$sql .= " t.facnumber,";
		$sql .= " t.entity,";
		$sql .= " t.ref_ext,";
		$sql .= " t.ref_int,";
		$sql .= " t.ref_client,";
		$sql .= " t.type,";
		$sql .= " t.increment,";
		$sql .= " t.fk_soc,";
		$sql .= " t.datec,";
		$sql .= " t.datef,";
		$sql .= " t.date_valid,";
		$sql .= " t.tms,";
		$sql .= " t.paye,";
		$sql .= " t.amount,";
		$sql .= " t.remise_percent,";
		$sql .= " t.remise_absolue,";
		$sql .= " t.remise,";
		$sql .= " t.close_code,";
		$sql .= " t.close_note,";
		$sql .= " t.tva,";
		$sql .= " t.localtax1,";
		$sql .= " t.localtax2,";
		$sql .= " t.revenuestamp,";
		$sql .= " t.total,";
		$sql .= " t.total_ttc,";
		$sql .= " t.fk_statut,";
		$sql .= " t.fk_user_author,";
		$sql .= " t.fk_user_modif,";
		$sql .= " t.fk_user_valid,";
		$sql .= " t.fk_facture_source,";
		$sql .= " t.fk_projet,";
		$sql .= " t.fk_account,";
		$sql .= " t.fk_currency,";
		$sql .= " t.fk_cond_reglement,";
		$sql .= " t.fk_mode_reglement,";
		$sql .= " t.date_lim_reglement,";
		$sql .= " t.note_private,";
		$sql .= " t.note_public,";
		$sql .= " t.model_pdf,";
		$sql .= " t.fk_incoterms,";
		$sql .= " t.location_incoterms,";
		$sql .= " t.situation_cycle_ref,";
		$sql .= " t.situation_counter,";
		$sql .= " t.situation_final,";
		$sql .= " t.import_key,";
		$sql .= " t.extraparams";

		
		$sql .= ' FROM ' . MAIN_DB_PREFIX . $this->table_element . ' as t';
		if (null !== $ref) {
			$sql .= ' WHERE t.ref = ' . '\'' . $ref . '\'';
		} else {
			$sql .= ' WHERE t.rowid = ' . $id;
		}

		$resql = $this->db->query($sql);
		if ($resql) {
			$numrows = $this->db->num_rows($resql);
			if ($numrows) {
				$obj = $this->db->fetch_object($resql);

				$this->id = $obj->rowid;
				
				$this->facnumber = $obj->facnumber;
				$this->entity = $obj->entity;
				$this->ref_ext = $obj->ref_ext;
				$this->ref_int = $obj->ref_int;
				$this->ref_client = $obj->ref_client;
				$this->type = $obj->type;
				$this->increment = $obj->increment;
				$this->fk_soc = $obj->fk_soc;
				$this->datec = $this->db->jdate($obj->datec);
				$this->datef = $this->db->jdate($obj->datef);
				$this->date_valid = $this->db->jdate($obj->date_valid);
				$this->tms = $this->db->jdate($obj->tms);
				$this->paye = $obj->paye;
				$this->amount = $obj->amount;
				$this->remise_percent = $obj->remise_percent;
				$this->remise_absolue = $obj->remise_absolue;
				$this->remise = $obj->remise;
				$this->close_code = $obj->close_code;
				$this->close_note = $obj->close_note;
				$this->tva = $obj->tva;
				$this->localtax1 = $obj->localtax1;
				$this->localtax2 = $obj->localtax2;
				$this->revenuestamp = $obj->revenuestamp;
				$this->total = $obj->total;
				$this->total_ttc = $obj->total_ttc;
				$this->fk_statut = $obj->fk_statut;
				$this->fk_user_author = $obj->fk_user_author;
				$this->fk_user_modif = $obj->fk_user_modif;
				$this->fk_user_valid = $obj->fk_user_valid;
				$this->fk_facture_source = $obj->fk_facture_source;
				$this->fk_projet = $obj->fk_projet;
				$this->fk_account = $obj->fk_account;
				$this->fk_currency = $obj->fk_currency;
				$this->fk_cond_reglement = $obj->fk_cond_reglement;
				$this->fk_mode_reglement = $obj->fk_mode_reglement;
				$this->date_lim_reglement = $this->db->jdate($obj->date_lim_reglement);
				$this->note_private = $obj->note_private;
				$this->note_public = $obj->note_public;
				$this->model_pdf = $obj->model_pdf;
				$this->fk_incoterms = $obj->fk_incoterms;
				$this->location_incoterms = $obj->location_incoterms;
				$this->situation_cycle_ref = $obj->situation_cycle_ref;
				$this->situation_counter = $obj->situation_counter;
				$this->situation_final = $obj->situation_final;
				$this->import_key = $obj->import_key;
				$this->extraparams = $obj->extraparams;

				
			}
			$this->db->free($resql);

			if ($numrows) {
				return 1;
			} else {
				return 0;
			}
		} else {
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);

			return - 1;
		}
	}

	/**
	 * Load object in memory from the database
	 *
	 * @param string $sortorder Sort Order
	 * @param string $sortfield Sort field
	 * @param int    $limit     offset limit
	 * @param int    $offset    offset limit
	 * @param array  $filter    filter array
	 * @param string $filtermode filter mode (AND or OR)
	 *
	 * @return int <0 if KO, >0 if OK
	 */
	public function fetchAll($sortorder='', $sortfield='', $limit=0, $offset=0, array $filter = array(), $filtermode='AND')
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$sql = 'SELECT';
		$sql .= ' t.rowid,';
		
		$sql .= " t.facnumber,";
		$sql .= " t.entity,";
		$sql .= " t.ref_ext,";
		$sql .= " t.ref_int,";
		$sql .= " t.ref_client,";
		$sql .= " t.type,";
		$sql .= " t.increment,";
		$sql .= " t.fk_soc,";
		$sql .= " t.datec,";
		$sql .= " t.datef,";
		$sql .= " t.date_valid,";
		$sql .= " t.tms,";
		$sql .= " t.paye,";
		$sql .= " t.amount,";
		$sql .= " t.remise_percent,";
		$sql .= " t.remise_absolue,";
		$sql .= " t.remise,";
		$sql .= " t.close_code,";
		$sql .= " t.close_note,";
		$sql .= " t.tva,";
		$sql .= " t.localtax1,";
		$sql .= " t.localtax2,";
		$sql .= " t.revenuestamp,";
		$sql .= " t.total,";
		$sql .= " t.total_ttc,";
		$sql .= " t.fk_statut,";
		$sql .= " t.fk_user_author,";
		$sql .= " t.fk_user_modif,";
		$sql .= " t.fk_user_valid,";
		$sql .= " t.fk_facture_source,";
		$sql .= " t.fk_projet,";
		$sql .= " t.fk_account,";
		$sql .= " t.fk_currency,";
		$sql .= " t.fk_cond_reglement,";
		$sql .= " t.fk_mode_reglement,";
		$sql .= " t.date_lim_reglement,";
		$sql .= " t.note_private,";
		$sql .= " t.note_public,";
		$sql .= " t.model_pdf,";
		$sql .= " t.fk_incoterms,";
		$sql .= " t.location_incoterms,";
		$sql .= " t.situation_cycle_ref,";
		$sql .= " t.situation_counter,";
		$sql .= " t.situation_final,";
		$sql .= " t.import_key,";
		$sql .= " t.extraparams";

		
		$sql .= ' FROM ' . MAIN_DB_PREFIX . $this->table_element. ' as t';

		// Manage filter
		$sqlwhere = array();
		if (count($filter) > 0) {
			foreach ($filter as $key => $value) {
				$sqlwhere [] = $key . ' LIKE \'%' . $this->db->escape($value) . '%\'';
			}
		}
		if (count($sqlwhere) > 0) {
			$sql .= ' WHERE ' . implode(' '.$filtermode.' ', $sqlwhere);
		}
		
		if (!empty($sortfield)) {
			$sql .= $this->db->order($sortfield,$sortorder);
		}
		if (!empty($limit)) {
		 $sql .=  ' ' . $this->db->plimit($limit + 1, $offset);
		}
		$this->lines = array();

		$resql = $this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);

			while ($obj = $this->db->fetch_object($resql)) {
				$line = new FactureLine();

				$line->id = $obj->rowid;
				
				$line->facnumber = $obj->facnumber;
				$line->entity = $obj->entity;
				$line->ref_ext = $obj->ref_ext;
				$line->ref_int = $obj->ref_int;
				$line->ref_client = $obj->ref_client;
				$line->type = $obj->type;
				$line->increment = $obj->increment;
				$line->fk_soc = $obj->fk_soc;
				$line->datec = $this->db->jdate($obj->datec);
				$line->datef = $this->db->jdate($obj->datef);
				$line->date_valid = $this->db->jdate($obj->date_valid);
				$line->tms = $this->db->jdate($obj->tms);
				$line->paye = $obj->paye;
				$line->amount = $obj->amount;
				$line->remise_percent = $obj->remise_percent;
				$line->remise_absolue = $obj->remise_absolue;
				$line->remise = $obj->remise;
				$line->close_code = $obj->close_code;
				$line->close_note = $obj->close_note;
				$line->tva = $obj->tva;
				$line->localtax1 = $obj->localtax1;
				$line->localtax2 = $obj->localtax2;
				$line->revenuestamp = $obj->revenuestamp;
				$line->total = $obj->total;
				$line->total_ttc = $obj->total_ttc;
				$line->fk_statut = $obj->fk_statut;
				$line->fk_user_author = $obj->fk_user_author;
				$line->fk_user_modif = $obj->fk_user_modif;
				$line->fk_user_valid = $obj->fk_user_valid;
				$line->fk_facture_source = $obj->fk_facture_source;
				$line->fk_projet = $obj->fk_projet;
				$line->fk_account = $obj->fk_account;
				$line->fk_currency = $obj->fk_currency;
				$line->fk_cond_reglement = $obj->fk_cond_reglement;
				$line->fk_mode_reglement = $obj->fk_mode_reglement;
				$line->date_lim_reglement = $this->db->jdate($obj->date_lim_reglement);
				$line->note_private = $obj->note_private;
				$line->note_public = $obj->note_public;
				$line->model_pdf = $obj->model_pdf;
				$line->fk_incoterms = $obj->fk_incoterms;
				$line->location_incoterms = $obj->location_incoterms;
				$line->situation_cycle_ref = $obj->situation_cycle_ref;
				$line->situation_counter = $obj->situation_counter;
				$line->situation_final = $obj->situation_final;
				$line->import_key = $obj->import_key;
				$line->extraparams = $obj->extraparams;

				

				$this->lines[] = $line;
			}
			$this->db->free($resql);

			return $num;
		} else {
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);

			return - 1;
		}
	}

	/**
	 * Update object into database
	 *
	 * @param  User $user      User that modifies
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 *
	 * @return int <0 if KO, >0 if OK
	 */
	public function update(User $user, $notrigger = false)
	{
		$error = 0;

		dol_syslog(__METHOD__, LOG_DEBUG);

		// Clean parameters
		
		if (isset($this->facnumber)) {
			 $this->facnumber = trim($this->facnumber);
		}
		if (isset($this->entity)) {
			 $this->entity = trim($this->entity);
		}
		if (isset($this->ref_ext)) {
			 $this->ref_ext = trim($this->ref_ext);
		}
		if (isset($this->ref_int)) {
			 $this->ref_int = trim($this->ref_int);
		}
		if (isset($this->ref_client)) {
			 $this->ref_client = trim($this->ref_client);
		}
		if (isset($this->type)) {
			 $this->type = trim($this->type);
		}
		if (isset($this->increment)) {
			 $this->increment = trim($this->increment);
		}
		if (isset($this->fk_soc)) {
			 $this->fk_soc = trim($this->fk_soc);
		}
		if (isset($this->paye)) {
			 $this->paye = trim($this->paye);
		}
		if (isset($this->amount)) {
			 $this->amount = trim($this->amount);
		}
		if (isset($this->remise_percent)) {
			 $this->remise_percent = trim($this->remise_percent);
		}
		if (isset($this->remise_absolue)) {
			 $this->remise_absolue = trim($this->remise_absolue);
		}
		if (isset($this->remise)) {
			 $this->remise = trim($this->remise);
		}
		if (isset($this->close_code)) {
			 $this->close_code = trim($this->close_code);
		}
		if (isset($this->close_note)) {
			 $this->close_note = trim($this->close_note);
		}
		if (isset($this->tva)) {
			 $this->tva = trim($this->tva);
		}
		if (isset($this->localtax1)) {
			 $this->localtax1 = trim($this->localtax1);
		}
		if (isset($this->localtax2)) {
			 $this->localtax2 = trim($this->localtax2);
		}
		if (isset($this->revenuestamp)) {
			 $this->revenuestamp = trim($this->revenuestamp);
		}
		if (isset($this->total)) {
			 $this->total = trim($this->total);
		}
		if (isset($this->total_ttc)) {
			 $this->total_ttc = trim($this->total_ttc);
		}
		if (isset($this->fk_statut)) {
			 $this->fk_statut = trim($this->fk_statut);
		}
		if (isset($this->fk_user_author)) {
			 $this->fk_user_author = trim($this->fk_user_author);
		}
		if (isset($this->fk_user_modif)) {
			 $this->fk_user_modif = trim($this->fk_user_modif);
		}
		if (isset($this->fk_user_valid)) {
			 $this->fk_user_valid = trim($this->fk_user_valid);
		}
		if (isset($this->fk_facture_source)) {
			 $this->fk_facture_source = trim($this->fk_facture_source);
		}
		if (isset($this->fk_projet)) {
			 $this->fk_projet = trim($this->fk_projet);
		}
		if (isset($this->fk_account)) {
			 $this->fk_account = trim($this->fk_account);
		}
		if (isset($this->fk_currency)) {
			 $this->fk_currency = trim($this->fk_currency);
		}
		if (isset($this->fk_cond_reglement)) {
			 $this->fk_cond_reglement = trim($this->fk_cond_reglement);
		}
		if (isset($this->fk_mode_reglement)) {
			 $this->fk_mode_reglement = trim($this->fk_mode_reglement);
		}
		if (isset($this->note_private)) {
			 $this->note_private = trim($this->note_private);
		}
		if (isset($this->note_public)) {
			 $this->note_public = trim($this->note_public);
		}
		if (isset($this->model_pdf)) {
			 $this->model_pdf = trim($this->model_pdf);
		}
		if (isset($this->fk_incoterms)) {
			 $this->fk_incoterms = trim($this->fk_incoterms);
		}
		if (isset($this->location_incoterms)) {
			 $this->location_incoterms = trim($this->location_incoterms);
		}
		if (isset($this->situation_cycle_ref)) {
			 $this->situation_cycle_ref = trim($this->situation_cycle_ref);
		}
		if (isset($this->situation_counter)) {
			 $this->situation_counter = trim($this->situation_counter);
		}
		if (isset($this->situation_final)) {
			 $this->situation_final = trim($this->situation_final);
		}
		if (isset($this->import_key)) {
			 $this->import_key = trim($this->import_key);
		}
		if (isset($this->extraparams)) {
			 $this->extraparams = trim($this->extraparams);
		}

		

		// Check parameters
		// Put here code to add a control on parameters values

		// Update request
		$sql = 'UPDATE ' . MAIN_DB_PREFIX . $this->table_element . ' SET';
		
		$sql .= ' facnumber = '.(isset($this->facnumber)?"'".$this->db->escape($this->facnumber)."'":"null").',';
		$sql .= ' entity = '.(isset($this->entity)?$this->entity:"null").',';
		$sql .= ' ref_ext = '.(isset($this->ref_ext)?"'".$this->db->escape($this->ref_ext)."'":"null").',';
		$sql .= ' ref_int = '.(isset($this->ref_int)?"'".$this->db->escape($this->ref_int)."'":"null").',';
		$sql .= ' ref_client = '.(isset($this->ref_client)?"'".$this->db->escape($this->ref_client)."'":"null").',';
		$sql .= ' type = '.(isset($this->type)?$this->type:"null").',';
		$sql .= ' increment = '.(isset($this->increment)?"'".$this->db->escape($this->increment)."'":"null").',';
		$sql .= ' fk_soc = '.(isset($this->fk_soc)?$this->fk_soc:"null").',';
		$sql .= ' datec = '.(! isset($this->datec) || dol_strlen($this->datec) != 0 ? "'".$this->db->idate($this->datec)."'" : 'null').',';
		$sql .= ' datef = '.(! isset($this->datef) || dol_strlen($this->datef) != 0 ? "'".$this->db->idate($this->datef)."'" : 'null').',';
		$sql .= ' date_valid = '.(! isset($this->date_valid) || dol_strlen($this->date_valid) != 0 ? "'".$this->db->idate($this->date_valid)."'" : 'null').',';
		$sql .= ' tms = '.(dol_strlen($this->tms) != 0 ? "'".$this->db->idate($this->tms)."'" : "'".$this->db->idate(dol_now())."'").',';
		$sql .= ' paye = '.(isset($this->paye)?$this->paye:"null").',';
		$sql .= ' amount = '.(isset($this->amount)?$this->amount:"null").',';
		$sql .= ' remise_percent = '.(isset($this->remise_percent)?$this->remise_percent:"null").',';
		$sql .= ' remise_absolue = '.(isset($this->remise_absolue)?$this->remise_absolue:"null").',';
		$sql .= ' remise = '.(isset($this->remise)?$this->remise:"null").',';
		$sql .= ' close_code = '.(isset($this->close_code)?"'".$this->db->escape($this->close_code)."'":"null").',';
		$sql .= ' close_note = '.(isset($this->close_note)?"'".$this->db->escape($this->close_note)."'":"null").',';
		$sql .= ' tva = '.(isset($this->tva)?$this->tva:"null").',';
		$sql .= ' localtax1 = '.(isset($this->localtax1)?$this->localtax1:"null").',';
		$sql .= ' localtax2 = '.(isset($this->localtax2)?$this->localtax2:"null").',';
		$sql .= ' revenuestamp = '.(isset($this->revenuestamp)?$this->revenuestamp:"null").',';
		$sql .= ' total = '.(isset($this->total)?$this->total:"null").',';
		$sql .= ' total_ttc = '.(isset($this->total_ttc)?$this->total_ttc:"null").',';
		$sql .= ' fk_statut = '.(isset($this->fk_statut)?$this->fk_statut:"null").',';
		$sql .= ' fk_user_author = '.(isset($this->fk_user_author)?$this->fk_user_author:"null").',';
		$sql .= ' fk_user_modif = '.(isset($this->fk_user_modif)?$this->fk_user_modif:"null").',';
		$sql .= ' fk_user_valid = '.(isset($this->fk_user_valid)?$this->fk_user_valid:"null").',';
		$sql .= ' fk_facture_source = '.(isset($this->fk_facture_source)?$this->fk_facture_source:"null").',';
		$sql .= ' fk_projet = '.(isset($this->fk_projet)?$this->fk_projet:"null").',';
		$sql .= ' fk_account = '.(isset($this->fk_account)?$this->fk_account:"null").',';
		$sql .= ' fk_currency = '.(isset($this->fk_currency)?"'".$this->db->escape($this->fk_currency)."'":"null").',';
		$sql .= ' fk_cond_reglement = '.(isset($this->fk_cond_reglement)?$this->fk_cond_reglement:"null").',';
		$sql .= ' fk_mode_reglement = '.(isset($this->fk_mode_reglement)?$this->fk_mode_reglement:"null").',';
		$sql .= ' date_lim_reglement = '.(! isset($this->date_lim_reglement) || dol_strlen($this->date_lim_reglement) != 0 ? "'".$this->db->idate($this->date_lim_reglement)."'" : 'null').',';
		$sql .= ' note_private = '.(isset($this->note_private)?"'".$this->db->escape($this->note_private)."'":"null").',';
		$sql .= ' note_public = '.(isset($this->note_public)?"'".$this->db->escape($this->note_public)."'":"null").',';
		$sql .= ' model_pdf = '.(isset($this->model_pdf)?"'".$this->db->escape($this->model_pdf)."'":"null").',';
		$sql .= ' fk_incoterms = '.(isset($this->fk_incoterms)?$this->fk_incoterms:"null").',';
		$sql .= ' location_incoterms = '.(isset($this->location_incoterms)?"'".$this->db->escape($this->location_incoterms)."'":"null").',';
		$sql .= ' situation_cycle_ref = '.(isset($this->situation_cycle_ref)?$this->situation_cycle_ref:"null").',';
		$sql .= ' situation_counter = '.(isset($this->situation_counter)?$this->situation_counter:"null").',';
		$sql .= ' situation_final = '.(isset($this->situation_final)?$this->situation_final:"null").',';
		$sql .= ' import_key = '.(isset($this->import_key)?"'".$this->db->escape($this->import_key)."'":"null").',';
		$sql .= ' extraparams = '.(isset($this->extraparams)?"'".$this->db->escape($this->extraparams)."'":"null");

        
		$sql .= ' WHERE rowid=' . $this->id;

		$this->db->begin();

		$resql = $this->db->query($sql);
		if (!$resql) {
			$error ++;
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
		}

		if (!$error && !$notrigger) {
			// Uncomment this and change MYOBJECT to your own tag if you
			// want this action calls a trigger.

			//// Call triggers
			//$result=$this->call_trigger('MYOBJECT_MODIFY',$user);
			//if ($result < 0) { $error++; //Do also what you must do to rollback action if trigger fail}
			//// End call triggers
		}

		// Commit or rollback
		if ($error) {
			$this->db->rollback();

			return - 1 * $error;
		} else {
			$this->db->commit();

			return 1;
		}
	}

	/**
	 * Delete object in database
	 *
	 * @param User $user      User that deletes
	 * @param bool $notrigger false=launch triggers after, true=disable triggers
	 *
	 * @return int <0 if KO, >0 if OK
	 */
	public function delete(User $user, $notrigger = false)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$error = 0;

		$this->db->begin();

		if (!$error) {
			if (!$notrigger) {
				// Uncomment this and change MYOBJECT to your own tag if you
				// want this action calls a trigger.

				//// Call triggers
				//$result=$this->call_trigger('MYOBJECT_DELETE',$user);
				//if ($result < 0) { $error++; //Do also what you must do to rollback action if trigger fail}
				//// End call triggers
			}
		}

		if (!$error) {
			$sql = 'DELETE FROM ' . MAIN_DB_PREFIX . $this->table_element;
			$sql .= ' WHERE rowid=' . $this->id;

			$resql = $this->db->query($sql);
			if (!$resql) {
				$error ++;
				$this->errors[] = 'Error ' . $this->db->lasterror();
				dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
			}
		}

		// Commit or rollback
		if ($error) {
			$this->db->rollback();

			return - 1 * $error;
		} else {
			$this->db->commit();

			return 1;
		}
	}

	/**
	 * Load an object from its id and create a new one in database
	 *
	 * @param int $fromid Id of object to clone
	 *
	 * @return int New id of clone
	 */
	public function createFromClone($fromid)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		global $user;
		$error = 0;
		$object = new Facture($this->db);

		$this->db->begin();

		// Load source object
		$object->fetch($fromid);
		// Reset object
		$object->id = 0;

		// Clear fields
		// ...

		// Create clone
		$result = $object->create($user);

		// Other options
		if ($result < 0) {
			$error ++;
			$this->errors = $object->errors;
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
		}

		// End
		if (!$error) {
			$this->db->commit();

			return $object->id;
		} else {
			$this->db->rollback();

			return - 1;
		}
	}

	/**
	 *  Return a link to the user card (with optionaly the picto)
	 * 	Use this->id,this->lastname, this->firstname
	 *
	 *	@param	int		$withpicto			Include picto in link (0=No picto, 1=Include picto into link, 2=Only picto)
	 *	@param	string	$option				On what the link point to
     *  @param	integer	$notooltip			1=Disable tooltip
     *  @param	int		$maxlen				Max length of visible user name
     *  @param  string  $morecss            Add more css on link
	 *	@return	string						String with URL
	 */
	function getNomUrl($withpicto=0, $option='', $notooltip=0, $maxlen=24, $morecss='')
	{
		global $langs, $conf, $db;
        global $dolibarr_main_authentication, $dolibarr_main_demo;
        global $menumanager;


        $result = '';
        $companylink = '';

        $label = '<u>' . $langs->trans("MyModule") . '</u>';
        $label.= '<div width="100%">';
        $label.= '<b>' . $langs->trans('Ref') . ':</b> ' . $this->ref;

        $link = '<a href="'.DOL_URL_ROOT.'/repartos/card.php?id='.$this->id.'"';
        $link.= ($notooltip?'':' title="'.dol_escape_htmltag($label, 1).'" class="classfortooltip'.($morecss?' '.$morecss:'').'"');
        $link.= '>';
		$linkend='</a>';

        if ($withpicto)
        {
            $result.=($link.img_object(($notooltip?'':$label), 'label', ($notooltip?'':'class="classfortooltip"')).$linkend);
            if ($withpicto != 2) $result.=' ';
		}
		$result.= $link . $this->ref . $linkend;
		return $result;
	}
	
	/**
	 *  Retourne le libelle du status d'un user (actif, inactif)
	 *
	 *  @param	int		$mode          0=libelle long, 1=libelle court, 2=Picto + Libelle court, 3=Picto, 4=Picto + Libelle long, 5=Libelle court + Picto
	 *  @return	string 			       Label of status
	 */
	function getLibStatut($mode=0)
	{
		return $this->LibStatut($this->status,$mode);
	}

	/**
	 *  Renvoi le libelle d'un status donne
	 *
	 *  @param	int		$status        	Id status
	 *  @param  int		$mode          	0=libelle long, 1=libelle court, 2=Picto + Libelle court, 3=Picto, 4=Picto + Libelle long, 5=Libelle court + Picto
	 *  @return string 			       	Label of status
	 */
	function LibStatut($status,$mode=0)
	{
		global $langs;

		if ($mode == 0)
		{
			$prefix='';
			if ($status == 1) return $langs->trans('Enabled');
			if ($status == 0) return $langs->trans('Disabled');
		}
		if ($mode == 1)
		{
			if ($status == 1) return $langs->trans('Enabled');
			if ($status == 0) return $langs->trans('Disabled');
		}
		if ($mode == 2)
		{
			if ($status == 1) return img_picto($langs->trans('Enabled'),'statut4').' '.$langs->trans('Enabled');
			if ($status == 0) return img_picto($langs->trans('Disabled'),'statut5').' '.$langs->trans('Disabled');
		}
		if ($mode == 3)
		{
			if ($status == 1) return img_picto($langs->trans('Enabled'),'statut4');
			if ($status == 0) return img_picto($langs->trans('Disabled'),'statut5');
		}
		if ($mode == 4)
		{
			if ($status == 1) return img_picto($langs->trans('Enabled'),'statut4').' '.$langs->trans('Enabled');
			if ($status == 0) return img_picto($langs->trans('Disabled'),'statut5').' '.$langs->trans('Disabled');
		}
		if ($mode == 5)
		{
			if ($status == 1) return $langs->trans('Enabled').' '.img_picto($langs->trans('Enabled'),'statut4');
			if ($status == 0) return $langs->trans('Disabled').' '.img_picto($langs->trans('Disabled'),'statut5');
		}
	}
	
	
	/**
	 * Initialise object with example values
	 * Id must be 0 if object instance is a specimen
	 *
	 * @return void
	 */
	public function initAsSpecimen()
	{
		$this->id = 0;
		
		$this->facnumber = '';
		$this->entity = '';
		$this->ref_ext = '';
		$this->ref_int = '';
		$this->ref_client = '';
		$this->type = '';
		$this->increment = '';
		$this->fk_soc = '';
		$this->datec = '';
		$this->datef = '';
		$this->date_valid = '';
		$this->tms = '';
		$this->paye = '';
		$this->amount = '';
		$this->remise_percent = '';
		$this->remise_absolue = '';
		$this->remise = '';
		$this->close_code = '';
		$this->close_note = '';
		$this->tva = '';
		$this->localtax1 = '';
		$this->localtax2 = '';
		$this->revenuestamp = '';
		$this->total = '';
		$this->total_ttc = '';
		$this->fk_statut = '';
		$this->fk_user_author = '';
		$this->fk_user_modif = '';
		$this->fk_user_valid = '';
		$this->fk_facture_source = '';
		$this->fk_projet = '';
		$this->fk_account = '';
		$this->fk_currency = '';
		$this->fk_cond_reglement = '';
		$this->fk_mode_reglement = '';
		$this->date_lim_reglement = '';
		$this->note_private = '';
		$this->note_public = '';
		$this->model_pdf = '';
		$this->fk_incoterms = '';
		$this->location_incoterms = '';
		$this->situation_cycle_ref = '';
		$this->situation_counter = '';
		$this->situation_final = '';
		$this->import_key = '';
		$this->extraparams = '';

		
	}

}

/**
 * Class FactureLine
 */
class FactureLine
{
	/**
	 * @var int ID
	 */
	public $id;
	/**
	 * @var mixed Sample line property 1
	 */
	
	public $facnumber;
	public $entity;
	public $ref_ext;
	public $ref_int;
	public $ref_client;
	public $type;
	public $increment;
	public $fk_soc;
	public $datec = '';
	public $datef = '';
	public $date_valid = '';
	public $tms = '';
	public $paye;
	public $amount;
	public $remise_percent;
	public $remise_absolue;
	public $remise;
	public $close_code;
	public $close_note;
	public $tva;
	public $localtax1;
	public $localtax2;
	public $revenuestamp;
	public $total;
	public $total_ttc;
	public $fk_statut;
	public $fk_user_author;
	public $fk_user_modif;
	public $fk_user_valid;
	public $fk_facture_source;
	public $fk_projet;
	public $fk_account;
	public $fk_currency;
	public $fk_cond_reglement;
	public $fk_mode_reglement;
	public $date_lim_reglement = '';
	public $note_private;
	public $note_public;
	public $model_pdf;
	public $fk_incoterms;
	public $location_incoterms;
	public $situation_cycle_ref;
	public $situation_counter;
	public $situation_final;
	public $import_key;
	public $extraparams;

	/**
	 * @var mixed Sample line property 2
	 */
	
}
