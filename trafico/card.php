<?php
$res=@include("../../main.inc.php");                                // For root directory
if (! $res) $res=include("../../../main.inc.php");  

require_once(DOL_DOCUMENT_ROOT."/core/class/html.formfile.class.php");
llxHeader("","Gesti&oacute;n",'');

$action 	= GETPOST('action');
$id 		= GETPOST('id');
$cancel     = GETPOST('cancel');
$backtopage = GETPOST('backtopage','alpha');
//print_r($_POST);
if( $action == 'add' && $_POST['ckfactura'] ) {
	
	$vehi 	= GETPOST('vehiculo');
	$conduc = GETPOST('conductor');

	$sql = "INSERT INTO llx_repartos_gestion (entity, fk_vehiculo, fk_conductor,status) 
			VALUES('".$conf->entity."','".$vehi."','".$conduc."','0')";
	$rs = $db->query($sql);
	$sql = "SELECT @@identity AS id";
	$rs = $db->query($sql);
	$rq = $db->fetch_object($rs);
	foreach ($_POST['ckfactura'] as &$valor) {
		//print $valor."::<br>";
		$sql = "INSERT INTO ".MAIN_DB_PREFIX."repartos_gestion_facture (entity, fk_gestion, fk_facture, completa) 
		  VALUES ('".$conf->entity."','".$rq->id."','".$valor."','0')";
		$rs = $db->query($sql);
	}
	print "<script>window.location.href='card.php?id=".$rq->id."';</script>";
}

if( $action == 'confirm_valid' ) {

	$sql = "SELECT fk_facture FROM ".MAIN_DB_PREFIX."repartos_gestion_facture WHERE fk_gestion=".$id." GROUP BY fk_facture";
	$rq = $db->query($sql);

	while( $rs = $db->fetch_object($rq) ) {
		
		$sql2="SELECT a.fk_product, a.qty, ifnull(b.cant,0) as cant, (a.qty-ifnull(b.cant,0)) as resta
			FROM ".MAIN_DB_PREFIX."facturedet a 
					LEFT JOIN (SELECT sum(qty) as cant,fk_product 
					FROM ".MAIN_DB_PREFIX."repartos_gestion_facturedet WHERE fk_facture=".$rs->fk_facture." 
				    GROUP BY fk_product) b ON a.fk_product=b.fk_product 
			WHERE a.fk_facture=".$rs->fk_facture." AND product_type=0 AND a.fk_product>0";
		
		$completa = 0;
		$rq2 = $db->query($sql2);

		while( $rs2=$db->fetch_object($rq2) ) {
			if( $rs2->resta > 0 ) {
				$completa = 1;
			}
		}

		if( $completa == 0 ) {
			$sqp = "UPDATE llx_repartos_gestion_facture SET completa=1 WHERE fk_facture=".$rs->fk_facture;
			$rq3 = $db->query($sqp);
		}
	}

	$sqp = "UPDATE ".MAIN_DB_PREFIX."repartos_gestion SET status=1 WHERE rowid=".$id;
	$rq3 = $db->query($sqp);
	print "<script>window.location.href='card.php?id=".$id."';</script>";
}

if( $action == '' && $id == '' ) {

	//print_fiche_titre('Nueva Gesti&oacute;n de tr&aacute;fico','','setup');
	$linkback="";
    print load_fiche_titre("Nuevo tr&aacute;fico",$linkback,'title_companies.png');

	print "<form method='POSt' action='card.php'>";

	dol_fiche_head(null, 'card', '', 0, '');

	print "<table class='border' width='100%'>";
	print "<input type='hidden' name='action' value='add'>";
	print "<tr>";
	$sql="SELECT rowid, idvehiculo,auto,modelo
		FROM ".MAIN_DB_PREFIX."repartos_vehiculos
		WHERE entity=".$conf->entity." ORDER BY idvehiculo";
	$rq=$db->query($sql);
	print "<td width='30%'>Veh&iacute;culo</td>";
	print "<td><select name='vehiculo'>";
	while($rs=$db->fetch_object($rq)){
		print "<option value='".$rs->rowid."'>".$rs->idvehiculo."-".$rs->auto." ".$rs->modelo."</option>";
	}
	print "</select></td></tr>";
	print "<tr>";
	$sql="SELECT rowid, idconductor, nombre
			FROM ".MAIN_DB_PREFIX."repartos_conductores
			WHERE entity=".$conf->entity." ORDER BY idconductor";
	$rq=$db->query($sql);
	print "<td width='30%'>Conductor</td>";
	print "<td><select name='conductor'>";
	while($rs=$db->fetch_object($rq)){
		print "<option value='".$rs->rowid."'>".$rs->idconductor."-".$rs->nombre."</option>";
	}
	print "</select></td></tr>";
	print "<tr>";
	print "<td>Facturas</td><td>";
	$sql="SELECT a.rowid, a.facnumber, a.fk_statut, d.canprod
			FROM ".MAIN_DB_PREFIX."facture a, 
				(SELECT count(c.rowid) as canprod,c.fk_facture 
					FROM ".MAIN_DB_PREFIX."facturedet c 
					WHERE fk_product>0 AND product_type=0 
					GROUP BY fk_facture) as d
			WHERE a.entity=".$conf->entity." AND (fk_statut!=0 AND fk_statut!=3) 
				AND a.rowid NOT IN (SELECT b.fk_facture 
							FROM ".MAIN_DB_PREFIX."repartos_gestion_facture b 
						    WHERE b.entity=".$conf->entity." AND b.completa=1) 
			    AND d.fk_facture=a.rowid ORDER BY a.facnumber";
	$rq=$db->query($sql);
	print "<table>";
	$formfile = new FormFile($db);
	while($rs=$db->fetch_object($rq)){
		/* $form = new Form($db);
		$text="Alfgo<br>lala<br>cant";
		$formconfirm=$form->form_confirm($_SERVER["PHP_SELF"], "Productos2", $text, 'confirm_aprobar', $formquestion, 0, 1, 220);
		print $formconfirm; */
		
		$filename=dol_sanitizeFileName($rs->facnumber);
		$filedir=$conf->facture->dir_output . '/' . dol_sanitizeFileName($rs->facnumber);
		$urlsource=$_SERVER['PHP_SELF'].'?facid='.$rs->rowid;
		$urlfac=DOL_MAIN_URL_ROOT.'/compta/facture.php?facid='.$rs->rowid;
		print "<tr><td><input type='checkbox' name='ckfactura[]' value='".$rs->rowid."'> <a href='".$urlfac."' target='_blank'>".$rs->facnumber." </a>";
		$formfile->show_documents('facture',$filename,$filedir,$urlsource,'','','',1,'',1);
		print "</td></tr>";
	}
	
	print "</table></td></tr>";
	//print "<tr>";
	//print "<td colspan='2' align='center'><input type='submit' name='add' value='Crear borrador'></td>";
	//print "</tr>";
	print "</table>";

	dol_fiche_end();

	print '<div class="center">';
	print '<input type="submit" class="button" name="add" value="Crear borrador">';
	if ($backtopage) {
	    print ' &nbsp; ';
	    //print '<input type="submit" class="button" name="cancel" value="'.$langs->trans('Cancel').'">';
	}
	print '</div>'."\n";
	print '</form>'."\n";
}

if( $action == '' && $id > 0 ) {

	if( GETPOST('addprod') ) {
		$sqd = "DELETE FROM ".MAIN_DB_PREFIX."repartos_gestion_facturedet WHERE fk_gestion=".$id;
		$rq = $db->query($sqd);
		foreach( $_POST as $nombre_campo => $valor ) {
		   //$asignacion = $nombre_campo . " = " . $valor;
		   //print $asignacion."<br><Br>";
		   $nom = explode("_", $nombre_campo);
		   if( $nom[0] == 'prod' ) {
		   		$fac 	= $nom[2];
		   		$prod 	= $nom[1];
		   		$disp 	= $_POST['disp_'.$nom[1]."_".$nom[2]];
		   		$cant 	= $_POST['cant_'.$nom[1]."_".$nom[2]];
		   		//print "F: ".$fac." P: ".$prod." D: ".$disp." C: ".$cant."<br><br>";
		   		if( $cant<=$disp ) {
			   		$sql = "INSERT INTO ".MAIN_DB_PREFIX."repartos_gestion_facturedet 
			   				(entity, fk_gestion, fk_facture, fk_product, qty)
			   				VALUES('".$conf->entity."','".$id."','".$fac."','".$prod."','".$cant."')";
			   		$rq = $db->query($sql);
		   		}
		   }
		}
	}
	
	//print_fiche_titre('Gesti&oacute;n de tr&aacute;fico','','setup');
	$linkback="";
    print load_fiche_titre("Gesti&oacute;n de tr&aacute;fico",$linkback,'title_companies.png');

	$sql="SELECT rowid, fk_vehiculo, fk_conductor,status
		FROM ".MAIN_DB_PREFIX."repartos_gestion
		WHERE entity=".$conf->entity." AND rowid=".$id;
	$rq1=$db->query($sql);
	$rs1=$db->fetch_object($rq);

	dol_fiche_head(null, 'card', '', 0, '');

	print "<table class='border' width='100%'>";
	print "<tr><td>Estatus</td><td>";
	if($rs1->status==0){print "Borrador";}
	if($rs1->status==1){print "Validado";}
	print "</td></tr>";
	print "<tr>";
	$sql="SELECT rowid, idvehiculo,auto,modelo,capacidad
		FROM ".MAIN_DB_PREFIX."repartos_vehiculos
		WHERE entity=".$conf->entity." AND rowid=".$rs1->fk_vehiculo;
	$rq=$db->query($sql);
	print "<td width='30%'>Veh&iacute;culo</td>";
	print "<td>";
	$rs=$db->fetch_object($rq);
		print "".$rs->idvehiculo." - ".$rs->auto." ".$rs->modelo."";
	print "</td></tr>";
	print "<tr>";
	print "<td width='30%'>Capacidad</td>";
	print "<td>";
	$capmax=$rs->capacidad;
	print "".$rs->capacidad."";
	print "</td></tr>";
	print "<tr>";
	$sql="SELECT rowid, idconductor, nombre
			FROM ".MAIN_DB_PREFIX."repartos_conductores
			WHERE entity=".$conf->entity." AND rowid=".$rs1->fk_conductor;
	$rq=$db->query($sql);
	print "<td width='30%'>Conductor</td>";
	print "<td>";
	$rs=$db->fetch_object($rq);
		print " ".$rs->idconductor." - ".$rs->nombre."";
	print "</td></tr>";
	print "<tr>";
	print "<td>Facturas</td><td>";
	$sql="SELECT a.rowid, a.facnumber, a.fk_statut
			FROM ".MAIN_DB_PREFIX."facture a
			WHERE a.entity=".$conf->entity." 
				AND a.rowid IN (SELECT b.fk_facture
							FROM ".MAIN_DB_PREFIX."repartos_gestion_facture b
						    WHERE b.entity=".$conf->entity." AND b.fk_gestion=".$id.")
			 ORDER BY a.facnumber";
	$rq=$db->query($sql);
	print "<table>";
	$formfile = new FormFile($db);
	$con=0;
	while($rs=$db->fetch_object($rq)){
		if($con=0){
			print "<tr>";
		}
		$filename=dol_sanitizeFileName($rs->facnumber);
		$filedir=$conf->facture->dir_output . '/' . dol_sanitizeFileName($rs->facnumber);
		$urlsource=$_SERVER['PHP_SELF'].'?facid='.$rs->rowid;
		$urlfac=DOL_MAIN_URL_ROOT.'/compta/facture.php?facid='.$rs->rowid;
		print "<td><a href='".$urlfac."' target='_blank'>".$rs->facnumber." </a>";
		$formfile->show_documents('facture',$filename,$filedir,$urlsource,'','','',1,'',1);
		print "</td>";
		$con++;
		if($con==5){
			print "</tr>";
			$con=0;
		}
	}
	
	print "</table></td></tr>";
	if($rs1->status==1){
		print "<tr class='liste_titre'><td colspan='2'>Productos Agregados</td></tr>";
		print "<tr><td colspan='2'><table  width='100%'>";
		print "<tr><td>Producto</td><td>Factura</td><td>Peso Unitario</td><td>Peso Total Asignado</td><td>Cant. Asignada</td></tr>";
		$sql="SELECT a.qty, b.facnumber, c.ref, c.label, weight
			FROM ".MAIN_DB_PREFIX."repartos_gestion_facturedet a, ".MAIN_DB_PREFIX."facture b, ".MAIN_DB_PREFIX."product c
			WHERE a.fk_gestion=".$id." AND a.fk_facture=b.rowid AND a.fk_product=c.rowid";
		$rq=$db->query($sql);
		$pesoglobal=0;
		while($rs=$db->fetch_object($rq)){
			$pesoglobal+=number_format($rs->weight*$rs->qty,2);
			print "<tr><td>".$rs->ref." - ".$rs->label."</td><td>".$rs->facnumber."</td>
					<td>".$rs->weight."</td><td>".number_format($rs->weight*$rs->qty,2)."</td>
					<td>".$rs->qty."</td></tr>";
		}
		$caprestante=$capmax-$pesoglobal;
		if($caprestante>=0){
			print "<tr><td colspan='3' align='right'>Total:</td><td>".$pesoglobal."</td>
					<td>Capacidad disponible ".$caprestante."</td></tr>";
		}else{
			print "<tr><td colspan='3' align='right'>Total:</td><td>".$pesoglobal."</td>
					<td><a style='color:red;'>Capacidad sobrepasada ".$caprestante."</a></td></tr>";
		}
		
		print "</table></td></tr>";
	}
	if($rs1->status==0){
	print "<tr class='liste_titre'><td colspan='2'>Agregar Productos</td></tr>";
	$sql="SELECT a.fk_facture, b.fk_product, b.qty,d.ref,d.label,d.weight,e.facnumber
			FROM ".MAIN_DB_PREFIX."repartos_gestion_facture a, 
			     ".MAIN_DB_PREFIX."facturedet b , ".MAIN_DB_PREFIX."product d,".MAIN_DB_PREFIX."facture e
			WHERE a.entity=".$conf->entity." AND a.fk_gestion=".$id." AND a.fk_facture=b.fk_facture 
					AND b.fk_product>0 
				AND product_type=0 AND b.fk_product=d.rowid AND a.fk_facture=e.rowid ORDER BY d.ref";
	//print $sql."<br>";
	$rq=$db->query($sql);
	print "<tr><td colspan='2'><table width='100%'>";
	print "<tr><td>Producto</td><td>Factura</td><td>Cant. Disponible</td><td>Peso Unitario</td><td>Peso Total Asignado</td><td>Cant. Asignada</td></tr>";
	$pesoglobal=0;
	print "<form method='POST' action='card.php?id=".$id."'>";
	while($rs=$db->fetch_object($rq)){
		/* print "<pre>";
		print_r($rs);
		print "</pre>"; */
		$disponible=$rs->qty;
		$sql2="SELECT sum(qty) as cant FROM ".MAIN_DB_PREFIX."repartos_gestion_facturedet 
				WHERE fk_facture=".$rs->fk_facture." AND fk_product=".$rs->fk_product;
		$rq2=$db->query($sql2);
		$rs2=$db->fetch_object($rq2);
		if($rs2->cant>0){
			$disponible-=$rs2->cant;
		}
		$sql3="SELECT sum(qty) as cant FROM ".MAIN_DB_PREFIX."repartos_gestion_facturedet
				WHERE fk_gestion=".$id." AND fk_facture=".$rs->fk_facture." AND fk_product=".$rs->fk_product;
		$rq3 = $db->query($sql3);
		$rs3 = $db->fetch_object($rq3);

		$asig 	= 0;
		$sel 	= '';
		$usado 	= $disponible;

		if($rs3->cant>0){
			$disponible+=$rs3->cant;
			$usado=$rs3->cant;
			$asig=1;
			$sel='checked';
		}
		if( $disponible == 0 ) {

		}
		else {
			$pestotal = number_format($rs->weight*$usado,2);
			if( $sel != '' ) {
				$pesoglobal+=$pestotal;
			}
			print "<tr><td><input type='checkbox' name='prod_".$rs->fk_product."_".$rs->fk_facture."' value='".$rs->fk_product."' ".$sel.">".$rs->ref." - ".$rs->label."</td>
					<td>".$rs->facnumber."</td>
					<td>".$disponible."</td>
					<td>".$rs->weight."</td>
					<td>".$pestotal."</td>";
			unset($sel);
			print "<td><input type='hidden' name='disp_".$rs->fk_product."_".$rs->fk_facture."' value='".$disponible."'><input type='text' name='cant_".$rs->fk_product."_".$rs->fk_facture."' value='".$usado."'></td></tr>";
		}
	}
	$caprestante=$capmax-$pesoglobal;
	print "<tr><td colspan='4' align='right'> Total seleccionado:</td><td>".$pesoglobal."</td>";
	if($caprestante>=0){
		print "<td> Capacidad disponible ".$caprestante."</td></tr>";
	}else{
		print "<td> <a style='color:red;'>Capacidad sobrepasada ".$caprestante."</a></td></tr>";
	}
	//print "<tr><td colspan='6' align='center'><input type='submit' name='addprod' value='Agregar/Actuaizar'></td></tr>";
	
	print "</table>";

	print "</td></tr>";
	}
	print "</table>";

	dol_fiche_end();

	print '<div class="center">';
	print '<input type="submit" class="button" name="addprod" value="Agregar/Actualizar">';
	if ($backtopage) {
	    print ' &nbsp; ';
	    //print '<input type="submit" class="button" name="cancel" value="'.$langs->trans('Cancel').'">';
	}
	print '</div>'."\n";
	print '</form>'."\n";


	if( $rs1->status == 0 && $pesoglobal != 0 ) {
		print '<div class="tabsAction">'."\n";
		print '<div class="inline-block divButAction"><a class="butAction" href="card.php?id='.$id.'&action2=conf_valid">Validar</a></div>';
		print '<div class="inline-block divButAction"><span id="action-delete" class="butActionDelete">'.$langs->trans('Delete').'</span></div>'."\n";
		print "</div>";
	}

	if( GETPOST('action2') == 'conf_valid' ) {
		$form = new Form($db);
		$text = "¿Desea validar la ord&eacute;n tr&aacute;fico?";
		$formconfirm=$form->form_confirm($_SERVER["PHP_SELF"]."?id=".$id, "Validar ord&eacute;n tr&aacute;fico", $text, 'confirm_valid', $formquestion, 0, 1, 220);
		print $formconfirm;
	}
}