<?php
$res=@include("../../main.inc.php");                                // For root directory
if (! $res) $res=include("../../../main.inc.php");  

llxHeader("","Lista de Conductores",'');
print_fiche_titre('Lista de Gesti&oacute;n de Tr&aacute;fico','','setup');

$sql="SELECT a.rowid, a.status, b.idvehiculo,b.auto,b.modelo, c.idconductor,c.nombre
		FROM ".MAIN_DB_PREFIX."repartos_gestion a, ".MAIN_DB_PREFIX."repartos_vehiculos b, ".MAIN_DB_PREFIX."repartos_conductores c
		WHERE a.entity=".$conf->entity." AND a.fk_vehiculo=b.rowid AND a.fk_conductor=c.rowid ORDER BY a.rowid DESC";
$rq=$db->query($sql);
print "<table class='border' width='100%'>";
print "<tr class='liste_titre'>";
	print "<td>Orden de Tr&aacute;fico</td>";
	print "<td>Estatus</td>";
	print "<td>Veh&iacute;culo</td>";
	print "<td>Conductor</td>";
print "</tr>";
while($rs=$db->fetch_object($rq)){
	print "<tr>";
		print "<td><a href='card.php?id=".$rs->rowid."'>".$rs->rowid."</a></td>";
		if($rs->status==0){$status= "Borrador";}
		if($rs->status==1){$status= "Validado";}
		print "<td>".$status."</td>";
		print "<td>".$rs->idvehiculo." - ".$rs->auto." ".$rs->modelo."</td>";
		print "<td>".$rs->idconductor." - ".$rs->nombre."</td>";
	print "</tr>";	
}

print "</table>";