
CREATE TABLE IF NOT EXISTS `llx_repartos_vehiculos` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `entity` int(11) DEFAULT '1',
  `status` int(11) DEFAULT '1',
  `idvehiculo` varchar(50) NOT NULL,
  `auto` varchar(150) NOT NULL,
  `modelo` varchar(150) NOT NULL,
  `placas` varchar(40) NOT NULL,
  `secompro` date NOT NULL,
  `capacidad` varchar(80) NOT NULL,
  `aseguradora` varchar(120) NOT NULL,
  `telefono` varchar(80) NOT NULL,
  `numpoliza` varchar(80) NOT NULL,
  `vencimiento` date NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
