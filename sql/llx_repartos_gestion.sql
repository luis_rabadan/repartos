CREATE TABLE IF NOT EXISTS `llx_repartos_gestion` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `entity` int(11) DEFAULT '1',
  `fk_vehiculo` int(11) DEFAULT '0',
  `fk_conductor` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`rowid`)
);

ALTER TABLE llx_repartos_gestion ADD COLUMN date datetime DEFAULT '0000-00-00 00:00:00';
