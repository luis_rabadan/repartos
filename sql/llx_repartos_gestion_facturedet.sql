CREATE TABLE IF NOT EXISTS `llx_repartos_gestion_facturedet` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `entity` int(11) DEFAULT '1',
  `fk_gestion` int(11) DEFAULT '0',
  `fk_facture` int(11) DEFAULT '0',
  `fk_product` int(11) DEFAULT '0',
  `qty` DOUBLE DEFAULT '0',
  PRIMARY KEY (`rowid`)
);
