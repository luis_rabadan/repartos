

CREATE TABLE IF NOT EXISTS `llx_repartos_conductores` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `entity` int(11) DEFAULT '1',
  `status` int(11) DEFAULT '1',
  `idconductor` varchar(50) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(40) NOT NULL,
  `email` varchar(128) NOT NULL,
  `depto` varchar(80) NOT NULL,
  `puesto` varchar(80) NOT NULL,
  `ingreso` date NOT NULL,
  `vlicencia` date NOT NULL,
  `cumpleanios` date NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
