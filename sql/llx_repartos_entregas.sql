CREATE TABLE IF NOT EXISTS `llx_repartos_entregas` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `entity` int(11) DEFAULT '1',
  `fk_gestion_facturedet` int(11) DEFAULT '0',
  `qty` int(11) DEFAULT '0',
  `comment` varchar(150) NOT NULL,
	`date` date NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `llx_repartos_entregas` MODIFY `date` datetime;