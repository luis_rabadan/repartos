<?php
$res=@include("../../main.inc.php");                                // For root directory
if (! $res) $res=include("../../../main.inc.php");  

llxHeader("","Lista de Conductores",'');
print_fiche_titre('Lista de Conductores','','setup');


print "<table class='noborder' width='100%'>";
	print "<tr class='liste_titre'>";
		print "<td width='20%'>ID</td>";
		print "<td width='20%'>Estatus</td>";
		print "<td width='20%'>Auto</td>";
		print "<td width='20%'>Modelo</td>";
		print "<td width='20%'>Placas</td>";
	print "</tr>";
	$sql="SELECT rowid,status, idvehiculo, auto, modelo, placas, secompro, capacidad, aseguradora,
				telefono, numpoliza, vencimiento
			FROM ".MAIN_DB_PREFIX."repartos_vehiculos
				WHERE entity=".$conf->entity." ORDER BY rowid DESC";
	$rs=$db->query($sql);
	
	while($rq=$db->fetch_object($rs)){
		print "<tr class='pair'>";
			print "<td><a href='card.php?id=".$rq->rowid."'><strong>".$rq->idvehiculo.img_view()."</strong></a></td>";
			$status='';
			if($rq->status==1){$status='Activo';}
			if($rq->status==2){$status='Baja';}
			print "<td>".$status."</td>";
			print "<td>".$rq->auto."</td>";
			print "<td>".$rq->modelo."</td>";
			print "<td>".$rq->placas."</td>";
		print "</tr>";
	}

print "</table>";