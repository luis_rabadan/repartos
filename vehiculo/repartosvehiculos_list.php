<?php
/* Copyright (C) 2007-2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *   	\file       repartos/repartosvehiculos_list.php
 *		\ingroup    repartos
 *		\brief      This file is an example of a php page
 *					Initialy built by build_class_from_table on 2016-12-21 18:26
 */

//if (! defined('NOREQUIREUSER'))  define('NOREQUIREUSER','1');
//if (! defined('NOREQUIREDB'))    define('NOREQUIREDB','1');
//if (! defined('NOREQUIRESOC'))   define('NOREQUIRESOC','1');
//if (! defined('NOREQUIRETRAN'))  define('NOREQUIRETRAN','1');
//if (! defined('NOCSRFCHECK'))    define('NOCSRFCHECK','1');			// Do not check anti CSRF attack test
//if (! defined('NOSTYLECHECK'))   define('NOSTYLECHECK','1');			// Do not check style html tag into posted data
//if (! defined('NOTOKENRENEWAL')) define('NOTOKENRENEWAL','1');		// Do not check anti POST attack test
//if (! defined('NOREQUIREMENU'))  define('NOREQUIREMENU','1');			// If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))  define('NOREQUIREHTML','1');			// If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))  define('NOREQUIREAJAX','1');
//if (! defined("NOLOGIN"))        define("NOLOGIN",'1');				// If this page is public (can be called outside logged session)

// Change this following line to use the correct relative path (../, ../../, etc)
$res=0;
if (! $res && file_exists("../main.inc.php")) $res=@include '../main.inc.php';					// to work if your module directory is into dolibarr root htdocs directory
if (! $res && file_exists("../../main.inc.php")) $res=@include '../../main.inc.php';			// to work if your module directory is into a subdir of root htdocs directory
if (! $res && file_exists("../../../dolibarr/htdocs/main.inc.php")) $res=@include '../../../dolibarr/htdocs/main.inc.php';     // Used on dev env only
if (! $res && file_exists("../../../../dolibarr/htdocs/main.inc.php")) $res=@include '../../../../dolibarr/htdocs/main.inc.php';   // Used on dev env only
if (! $res) die("Include of main fails");
// Change this following line to use the correct relative path from htdocs
include_once(DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php');
dol_include_once('/repartos/class/repartosvehiculos.class.php');

// Load traductions files requiredby by page

$langs->load("other");
$langs->load("repartos@repartos");

// Get parameters
$id			= GETPOST('id','int');
$action		= GETPOST('action','alpha');
$backtopage = GETPOST('backtopage');
$myparam	= GETPOST('myparam','alpha');


$search_entity=GETPOST('search_entity','int');
$search_idvehiculo=GETPOST('search_idvehiculo','alpha');
$search_status=GETPOST('search_status','int');
$search_auto=GETPOST('search_auto','alpha');
$search_modelo=GETPOST('search_modelo','alpha');
$search_placas=GETPOST('search_placas','alpha');
$search_capacidad=GETPOST('search_capacidad','alpha');
$search_aseguradora=GETPOST('search_aseguradora','alpha');
$search_telefono=GETPOST('search_telefono','alpha');
$search_numpoliza=GETPOST('search_numpoliza','alpha');


$optioncss = GETPOST('optioncss','alpha');

// Load variable for pagination
$limit = GETPOST("limit")?GETPOST("limit","int"):$conf->liste_limit;
$sortfield = GETPOST('sortfield','alpha');
$sortorder = GETPOST('sortorder','alpha');
$page = GETPOST('page','int');
if ($page == -1) { $page = 0; }
$offset = $limit * $page;
$pageprev = $page - 1;
$pagenext = $page + 1;
if (! $sortfield) $sortfield="t.rowid"; // Set here default search field
if (! $sortorder) $sortorder="ASC";

// Protection if external user
$socid=0;
if ($user->societe_id > 0)
{
    $socid = $user->societe_id;
	//accessforbidden();
}

// Initialize technical object to manage hooks. Note that conf->hooks_modules contains array
$hookmanager->initHooks(array('repartosvehiculoslist'));
$extrafields = new ExtraFields($db);

// fetch optionals attributes and labels
$extralabels = $extrafields->fetch_name_optionals_label('repartos');
$search_array_options=$extrafields->getOptionalsFromPost($extralabels,'','search_');

// Load object if id or ref is provided as parameter
$object=new Repartosvehiculos($db);
if (($id > 0 || ! empty($ref)) && $action != 'add')
{
	$result=$object->fetch($id,$ref);
	if ($result < 0) dol_print_error($db);
}

// Definition of fields for list
$arrayfields=array(
    
//'t.entity'=>array('label'=>$langs->trans("Fieldentity"), 'checked'=>1),
't.idvehiculo'=>array('label'=>$langs->trans("rep_idvehiculo"), 'checked'=>1),
't.status'=>array('label'=>$langs->trans("rep_status"), 'checked'=>1),
't.auto'=>array('label'=>$langs->trans("rep_auto"), 'checked'=>1),
't.modelo'=>array('label'=>$langs->trans("rep_modelo"), 'checked'=>1),
't.placas'=>array('label'=>$langs->trans("rep_placas"), 'checked'=>1),
't.capacidad'=>array('label'=>$langs->trans("rep_capacidad"), 'checked'=>1),
't.aseguradora'=>array('label'=>$langs->trans("rep_segure"), 'checked'=>1),
't.telefono'=>array('label'=>$langs->trans("rep_phone"), 'checked'=>1),
't.numpoliza'=>array('label'=>$langs->trans("rep_numPoli"), 'checked'=>1),

    
    //'t.entity'=>array('label'=>$langs->trans("Entity"), 'checked'=>1, 'enabled'=>(! empty($conf->multicompany->enabled) && empty($conf->multicompany->transverse_mode))),
   // 't.datec'=>array('label'=>$langs->trans("DateCreation"), 'checked'=>0, 'position'=>500),
    //'t.tms'=>array('label'=>$langs->trans("DateModificationShort"), 'checked'=>0, 'position'=>500),
    //'t.statut'=>array('label'=>$langs->trans("Status"), 'checked'=>1, 'position'=>1000),
);
// Extra fields
if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
{
   foreach($extrafields->attribute_label as $key => $val) 
   {
       $arrayfields["ef.".$key]=array('label'=>$extrafields->attribute_label[$key], 'checked'=>$extrafields->attribute_list[$key], 'position'=>$extrafields->attribute_pos[$key], 'enabled'=>$extrafields->attribute_perms[$key]);
   }
}




/*******************************************************************
* ACTIONS
*
* Put here all code to do according to value of "action" parameter
********************************************************************/

$parameters=array();
$reshook=$hookmanager->executeHooks('doActions',$parameters,$object,$action);    // Note that $action and $object may have been modified by some hooks
if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

include DOL_DOCUMENT_ROOT.'/core/actions_changeselectedfields.inc.php';

if (GETPOST("button_removefilter_x") || GETPOST("button_removefilter.x") ||GETPOST("button_removefilter")) // All test are required to be compatible with all browsers
{
	
$search_entity='';
$search_status='';
$search_idvehiculo='';
$search_auto='';
$search_modelo='';
$search_placas='';
$search_capacidad='';
$search_aseguradora='';
$search_telefono='';
$search_numpoliza='';

	
	$search_date_creation='';
	$search_date_update='';
	$search_array_options=array();
}


if (empty($reshook))
{
	// Action to delete
	if ($action == 'confirm_delete')
	{
		$result=$object->delete($user);
		if ($result > 0)
		{
			// Delete OK
			setEventMessages("RecordDeleted", null, 'mesgs');
			header("Location: ".dol_buildpath('/repartos/list.php',1));
			exit;
		}
		else
		{
			if (! empty($object->errors)) setEventMessages(null,$object->errors,'errors');
			else setEventMessages($object->error,null,'errors');
		}
	}
}




/***************************************************
* VIEW
*
* Put here all code to build page
****************************************************/


llxHeader('',$langs->trans('rep_titre'),'');

$form=new Form($db);

// Put here content of your page
$title = $langs->trans('rep_list_vehiculo');

// Example : Adding jquery code
print '<script type="text/javascript" language="javascript">
jQuery(document).ready(function() {
	function init_myfunc()
	{
		jQuery("#myid").removeAttr(\'disabled\');
		jQuery("#myid").attr(\'disabled\',\'disabled\');
	}
	init_myfunc();
	jQuery("#mybutton").click(function() {
		init_myfunc();
	});
});
</script>';


$sql = "SELECT";
$sql.= " t.rowid,";

		$sql .= " t.entity,";
		$sql .= " t.status,";
		$sql .= " t.idvehiculo,";
		$sql .= " t.auto,";
		$sql .= " t.modelo,";
		$sql .= " t.placas,";
		$sql .= " t.secompro,";
		$sql .= " t.capacidad,";
		$sql .= " t.aseguradora,";
		$sql .= " t.telefono,";
		$sql .= " t.numpoliza,";
		$sql .= " t.vencimiento";


// Add fields for extrafields
foreach ($extrafields->attribute_list as $key => $val) $sql.=",ef.".$key.' as options_'.$key;
// Add fields from hooks
$parameters=array();
$reshook=$hookmanager->executeHooks('printFieldListSelect',$parameters);    // Note that $action and $object may have been modified by hook
$sql.=$hookmanager->resPrint;
$sql.= " FROM ".MAIN_DB_PREFIX."repartos_vehiculos as t";
if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label)) $sql.= " LEFT JOIN ".MAIN_DB_PREFIX."repartos_vehiculos_extrafields as ef on (u.rowid = ef.fk_object)";
$sql.= " WHERE 1 = 1";
//$sql.= " WHERE u.entity IN (".getEntity('mytable',1).")";

//if ($search_entity) $sql.= natural_search("entity",$search_entity);
if ($search_status) $sql.= natural_search("status",$search_status);
if ($search_idvehiculo) $sql.= natural_search("idvehiculo",$search_idvehiculo);
if ($search_auto) $sql.= natural_search("auto",$search_auto);
if ($search_modelo) $sql.= natural_search("modelo",$search_modelo);
if ($search_placas) $sql.= natural_search("placas",$search_placas);
if ($search_capacidad) $sql.= natural_search("capacidad",$search_capacidad);
if ($search_aseguradora) $sql.= natural_search("aseguradora",$search_aseguradora);
if ($search_telefono) $sql.= natural_search("telefono",$search_telefono);
if ($search_numpoliza) $sql.= natural_search("numpoliza",$search_numpoliza);


if ($sall)          $sql.= natural_search(array_keys($fieldstosearchall), $sall);
// Add where from extra fields
foreach ($search_array_options as $key => $val)
{
    $crit=$val;
    $tmpkey=preg_replace('/search_options_/','',$key);
    $typ=$extrafields->attribute_type[$tmpkey];
    $mode=0;
    if (in_array($typ, array('int','double'))) $mode=1;    // Search on a numeric
    if ($val && ( ($crit != '' && ! in_array($typ, array('select'))) || ! empty($crit))) 
    {
        $sql .= natural_search('ef.'.$tmpkey, $crit, $mode);
    }
}
// Add where from hooks
$parameters=array();
$reshook=$hookmanager->executeHooks('printFieldListWhere',$parameters);    // Note that $action and $object may have been modified by hook
$sql.=$hookmanager->resPrint;
$sql.=$db->order($sortfield,$sortorder);
//$sql.= $db->plimit($conf->liste_limit+1, $offset);

// Count total nb of records
$nbtotalofrecords = 0;
if (empty($conf->global->MAIN_DISABLE_FULL_SCANLIST))
{
	$result = $db->query($sql);
	$nbtotalofrecords = $db->num_rows($result);
}	

$sql.= $db->plimit($conf->liste_limit+1, $offset);


dol_syslog($script_file, LOG_DEBUG);
$resql=$db->query($sql);
if ($resql)
{
    $num = $db->num_rows($resql);
    
    $params='';
	
//if ($search_entity != '') $params.= '&amp;search_entity='.urlencode($search_entity);
if ($search_idvehiculo != '') $params.= '&amp;search_idvehiculo='.urlencode($search_idvehiculo);
if ($search_status != '') $params.= '&amp;search_status='.urlencode($search_status);
if ($search_auto != '') $params.= '&amp;search_auto='.urlencode($search_auto);
if ($search_modelo != '') $params.= '&amp;search_modelo='.urlencode($search_modelo);
if ($search_placas != '') $params.= '&amp;search_placas='.urlencode($search_placas);
if ($search_capacidad != '') $params.= '&amp;search_capacidad='.urlencode($search_capacidad);
if ($search_aseguradora != '') $params.= '&amp;search_aseguradora='.urlencode($search_aseguradora);
if ($search_telefono != '') $params.= '&amp;search_telefono='.urlencode($search_telefono);
if ($search_numpoliza != '') $params.= '&amp;search_numpoliza='.urlencode($search_numpoliza);

	
    if ($optioncss != '') $param.='&optioncss='.$optioncss;
    // Add $param from extra fields
    foreach ($search_array_options as $key => $val)
    {
        $crit=$val;
        $tmpkey=preg_replace('/search_options_/','',$key);
        if ($val != '') $param.='&search_options_'.$tmpkey.'='.urlencode($val);
    } 
    
    print_barre_liste($title, $page, $_SERVER["PHP_SELF"],$params,$sortfield,$sortorder,'',$num,$nbtotalofrecords,'title_companies');
    

	print '<form method="GET" id="searchFormList" action="'.$_SERVER["PHP_SELF"].'">';
    if ($optioncss != '') print '<input type="hidden" name="optioncss" value="'.$optioncss.'">';
	print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
	print '<input type="hidden" name="formfilteraction" id="formfilteraction" value="list">';
	print '<input type="hidden" name="sortfield" value="'.$sortfield.'">';
	print '<input type="hidden" name="sortorder" value="'.$sortorder.'">';
	
    if ($sall)
    {
        foreach($fieldstosearchall as $key => $val) $fieldstosearchall[$key]=$langs->trans($val);
        print $langs->trans("FilterOnInto", $all) . join(', ',$fieldstosearchall);
    }
    
	if (! empty($moreforfilter))
	{
		print '<div class="liste_titre liste_titre_bydiv centpercent">';
		print $moreforfilter;
    	$parameters=array();
    	$reshook=$hookmanager->executeHooks('printFieldPreListTitle',$parameters);    // Note that $action and $object may have been modified by hook
	    print $hookmanager->resPrint;
	    print '</div>';
	}

    $varpage=empty($contextpage)?$_SERVER["PHP_SELF"]:$contextpage;
    $selectedfields=$form->multiSelectArrayWithCheckbox('selectedfields', $arrayfields, $varpage);	// This also change content of $arrayfields
	
	print '<table class="liste '.($moreforfilter?"listwithfilterbefore":"").'">';

    // Fields title
    print '<tr class="liste_titre">';
    
//if (! empty($arrayfields['t.entity']['checked'])) print_liste_field_titre($arrayfields['t.entity']['label'],$_SERVER['PHP_SELF'],'t.entity','',$param,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.idvehiculo']['checked'])) print_liste_field_titre($arrayfields['t.idvehiculo']['label'],$_SERVER['PHP_SELF'],'t.idvehiculo','',$param,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.status']['checked'])) print_liste_field_titre($arrayfields['t.status']['label'],$_SERVER['PHP_SELF'],'t.status','',$param,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.auto']['checked'])) print_liste_field_titre($arrayfields['t.auto']['label'],$_SERVER['PHP_SELF'],'t.auto','',$param,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.modelo']['checked'])) print_liste_field_titre($arrayfields['t.modelo']['label'],$_SERVER['PHP_SELF'],'t.modelo','',$param,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.placas']['checked'])) print_liste_field_titre($arrayfields['t.placas']['label'],$_SERVER['PHP_SELF'],'t.placas','',$param,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.capacidad']['checked'])) print_liste_field_titre($arrayfields['t.capacidad']['label'],$_SERVER['PHP_SELF'],'t.capacidad','',$param,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.aseguradora']['checked'])) print_liste_field_titre($arrayfields['t.aseguradora']['label'],$_SERVER['PHP_SELF'],'t.aseguradora','',$param,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.telefono']['checked'])) print_liste_field_titre($arrayfields['t.telefono']['label'],$_SERVER['PHP_SELF'],'t.telefono','',$param,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.numpoliza']['checked'])) print_liste_field_titre($arrayfields['t.numpoliza']['label'],$_SERVER['PHP_SELF'],'t.numpoliza','',$param,'',$sortfield,$sortorder);

    
	// Extra fields
	if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
	{
	   foreach($extrafields->attribute_label as $key => $val) 
	   {
           if (! empty($arrayfields["ef.".$key]['checked'])) 
           {
				$align=$extrafields->getAlignFlag($key);
				print_liste_field_titre($extralabels[$key],$_SERVER["PHP_SELF"],"ef.".$key,"",$param,($align?'align="'.$align.'"':''),$sortfield,$sortorder);
           }
	   }
	}
    // Hook fields
	$parameters=array('arrayfields'=>$arrayfields);
    $reshook=$hookmanager->executeHooks('printFieldListTitle',$parameters);    // Note that $action and $object may have been modified by hook
    print $hookmanager->resPrint;
	if (! empty($arrayfields['t.datec']['checked']))  print_liste_field_titre($langs->trans("DateCreationShort"),$_SERVER["PHP_SELF"],"t.datec","",$param,'align="center" class="nowrap"',$sortfield,$sortorder);
	if (! empty($arrayfields['t.tms']['checked']))    print_liste_field_titre($langs->trans("DateModificationShort"),$_SERVER["PHP_SELF"],"t.tms","",$param,'align="center" class="nowrap"',$sortfield,$sortorder);
	//if (! empty($arrayfields['t.status']['checked'])) print_liste_field_titre($langs->trans("Status"),$_SERVER["PHP_SELF"],"t.status","",$param,'align="center"',$sortfield,$sortorder);
	print_liste_field_titre($selectedfields, $_SERVER["PHP_SELF"],"",'','','align="right"',$sortfield,$sortorder,'maxwidthsearch ');
    print '</tr>'."\n";

    // Fields title search
	print '<tr class="liste_titre">';
	
	//if (! empty($arrayfields['t.entity']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_entity" value="'.$search_entity.'" size="10"></td>';
	if (! empty($arrayfields['t.idvehiculo']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_idvehiculo" value="'.$search_idvehiculo.'" size="10"></td>';
	if (! empty($arrayfields['t.status']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_status" value="'.$search_status.'" size="10"></td>';	
	if (! empty($arrayfields['t.auto']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_auto" value="'.$search_auto.'" size="10"></td>';
	if (! empty($arrayfields['t.modelo']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_modelo" value="'.$search_modelo.'" size="10"></td>';
	if (! empty($arrayfields['t.placas']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_placas" value="'.$search_placas.'" size="10"></td>';
	if (! empty($arrayfields['t.capacidad']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_capacidad" value="'.$search_capacidad.'" size="10"></td>';
	if (! empty($arrayfields['t.aseguradora']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_aseguradora" value="'.$search_aseguradora.'" size="10"></td>';
	if (! empty($arrayfields['t.telefono']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_telefono" value="'.$search_telefono.'" size="10"></td>';
	if (! empty($arrayfields['t.numpoliza']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_numpoliza" value="'.$search_numpoliza.'" size="10"></td>';

		
	// Extra fields
	if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
	{
        foreach($extrafields->attribute_label as $key => $val) 
        {
            if (! empty($arrayfields["ef.".$key]['checked']))
            {
                $align=$extrafields->getAlignFlag($key);
                $typeofextrafield=$extrafields->attribute_type[$key];
                print '<td class="liste_titre'.($align?' '.$align:'').'">';
            	if (in_array($typeofextrafield, array('varchar', 'int', 'double', 'select')))
				{
				    $crit=$val;
    				$tmpkey=preg_replace('/search_options_/','',$key);
    				$searchclass='';
    				if (in_array($typeofextrafield, array('varchar', 'select'))) $searchclass='searchstring';
    				if (in_array($typeofextrafield, array('int', 'double'))) $searchclass='searchnum';
    				print '<input class="flat'.($searchclass?' '.$searchclass:'').'" size="4" type="text" name="search_options_'.$tmpkey.'" value="'.dol_escape_htmltag($search_array_options['search_options_'.$tmpkey]).'">';
				}
                print '</td>';
            }
        }
	}
    // Fields from hook
	$parameters=array('arrayfields'=>$arrayfields);
    $reshook=$hookmanager->executeHooks('printFieldListOption',$parameters);    // Note that $action and $object may have been modified by hook
    print $hookmanager->resPrint;
    if (! empty($arrayfields['t.datec']['checked']))
    {
        // Date creation
        print '<td class="liste_titre">';
        print '</td>';
    }
    if (! empty($arrayfields['t.tms']['checked']))
    {
        // Date modification
        print '<td class="liste_titre">';
        print '</td>';
    }
    /*if (! empty($arrayfields['u.statut']['checked']))
    {
        // Status
        print '<td class="liste_titre" align="center">';
        print $form->selectarray('search_statut', array('-1'=>'','0'=>$langs->trans('Disabled'),'1'=>$langs->trans('Enabled')),$search_statut);
        print '</td>';
    }*/
    // Action column
	print '<td class="liste_titre" align="right">';
	print '<input type="image" class="liste_titre" name="button_search" src="'.img_picto($langs->trans("Search"),'search.png','','',1).'" value="'.dol_escape_htmltag($langs->trans("Search")).'" title="'.dol_escape_htmltag($langs->trans("Search")).'">';
	print '<input type="image" class="liste_titre" name="button_removefilter" src="'.img_picto($langs->trans("Search"),'searchclear.png','','',1).'" value="'.dol_escape_htmltag($langs->trans("RemoveFilter")).'" title="'.dol_escape_htmltag($langs->trans("RemoveFilter")).'">';
	print '</td>';
	print '</tr>'."\n";
        
    
    $i = 0;
    while ($i < $num)
    {
        $obj = $db->fetch_object($resql);
        if ($obj)
        {
            // You can use here results
            print '<tr>';
            
            $objVehiculo= new Repartosvehiculos($db);
            $objVehiculo->fetch($obj->rowid);

			//if (! empty($arrayfields['t.entity']['checked'])) print '<td>'.$obj->entity.'</td>';
			
			if (! empty($arrayfields['t.idvehiculo']['checked'])) print '<td>'.$objVehiculo->getNomUrl(1).'</td>';
			if (! empty($arrayfields['t.status']['checked'])) print '<td>'.$langs->trans('rep_status'.$obj->status).'</td>';
			if (! empty($arrayfields['t.auto']['checked'])) print '<td>'.$obj->auto.'</td>';
			if (! empty($arrayfields['t.modelo']['checked'])) print '<td>'.$obj->modelo.'</td>';
			if (! empty($arrayfields['t.placas']['checked'])) print '<td>'.$obj->placas.'</td>';
			if (! empty($arrayfields['t.capacidad']['checked'])) print '<td>'.$obj->capacidad.'</td>';
			if (! empty($arrayfields['t.aseguradora']['checked'])) print '<td>'.$obj->aseguradora.'</td>';
			if (! empty($arrayfields['t.telefono']['checked'])) print '<td>'.$obj->telefono.'</td>';
			if (! empty($arrayfields['t.numpoliza']['checked'])) print '<td>'.$obj->numpoliza.'</td>';

            
        	// Extra fields
    		if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
    		{
    		   foreach($extrafields->attribute_label as $key => $val) 
    		   {
    				if (! empty($arrayfields["ef.".$key]['checked'])) 
    				{
    					print '<td';
    					$align=$extrafields->getAlignFlag($key);
    					if ($align) print ' align="'.$align.'"';
    					print '>';
    					$tmpkey='options_'.$key;
    					print $extrafields->showOutputField($key, $obj->$tmpkey, '', 1);
    					print '</td>';
    				}
    		   }
    		}
            // Fields from hook
    	    $parameters=array('arrayfields'=>$arrayfields, 'obj'=>$obj);
    		$reshook=$hookmanager->executeHooks('printFieldListValue',$parameters);    // Note that $action and $object may have been modified by hook
            print $hookmanager->resPrint;
        	// Date creation
            if (! empty($arrayfields['t.datec']['checked']))
            {
                print '<td align="center">';
                print dol_print_date($db->jdate($obj->date_creation), 'dayhour');
                print '</td>';
            }
            // Date modification
            if (! empty($arrayfields['t.tms']['checked']))
            {
                print '<td align="center">';
                print dol_print_date($db->jdate($obj->date_update), 'dayhour');
                print '</td>';
            }
            // Status
            /*
            if (! empty($arrayfields['u.statut']['checked']))
            {
    		  $userstatic->statut=$obj->statut;
              print '<td align="center">'.$userstatic->getLibStatut(3).'</td>';
            }*/
            // Action column
            print '<td></td>';
    		print '</tr>';
        }
        $i++;
    }
    
    $db->free($resql);

	$parameters=array('sql' => $sql);
	$reshook=$hookmanager->executeHooks('printFieldListFooter',$parameters);    // Note that $action and $object may have been modified by hook
	print $hookmanager->resPrint;

	print "</table>\n";
	print "</form>\n";
	
	$db->free($result);
}
else
{
    $error++;
    dol_print_error($db);
}


// End of page
llxFooter();
$db->close();
