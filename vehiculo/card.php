<?php
$res=@include("../../main.inc.php");                                // For root directory
if (! $res) $res=include("../../../main.inc.php");  

llxHeader("","Veh&iacute;culo",'');

$action 	= GETPOST('action');
$idvei 		= GETPOST('id');
$cancel     = GETPOST('cancel');
$backtopage = GETPOST('backtopage','alpha');

if( $action == 'add' ) {
	
	$status 		= GETPOST('status');
	$idv 			= GETPOST('idv');
	$auto 			= GETPOST('auto');
	$modelo 		= GETPOST('modelo');
	$placas 		= GETPOST('placas');
	$sc 			= str_replace("/","-", $_POST['secompro']);
	$secompro 		= date('Y-m-d',strtotime($sc));
	$capacidad 		= GETPOST('capacidad');
	$aseguradora 	= GETPOST('aseguradora');
	$telefono 		= GETPOST('telefono');
	$numpoliza 		= GETPOST('numpoliza');
	$vc 			= str_replace("/","-", $_POST['vencimiento']);
	$vencimiento 	= date('Y-m-d',strtotime($vc));
	
	$sql="INSERT INTO ".MAIN_DB_PREFIX."repartos_vehiculos (entity, status, idvehiculo, auto, modelo, placas,  
		secompro, capacidad, aseguradora, telefono, numpoliza, vencimiento)
		VALUES('".$conf->entity."','".$status."','".$idv."','".$auto."','".$modelo."','".$placas."'
		,'".$secompro."','".$capacidad."','".$aseguradora."','".$telefono."','".$numpoliza."','".$vencimiento."')";
	//print $sql;
	if( $rs = $db->query($sql) ) {
		$sql = "SELECT @@identity AS id";
		$rs = $db->query($sql);
		$rq = $db->fetch_object($rs);
		//print $rq->id;
		print "<script>window.location.href='card.php?id=".$rq->id."';</script>";
	}
	else {
		//dol_htmloutput_mesg('');
		dol_htmloutput_errors('Error al registrar el veh&iacute;culo');
	}
}

if( $action == 'confedit' ) {
	
	$status 		= GETPOST('status');
	$idv 			= GETPOST('idv');
	$auto 			= GETPOST('auto');
	$modelo 		= GETPOST('modelo');
	$placas 		= GETPOST('placas');
	$sc 			= str_replace("/","-", $_POST['secompro']);
	$secompro 		= date('Y-m-d',strtotime($sc));
	$capacidad 		= GETPOST('capacidad');
	$aseguradora 	= GETPOST('aseguradora');
	$telefono 		= GETPOST('telefono');
	$numpoliza 		= GETPOST('numpoliza');
	$vc 			= str_replace("/","-", $_POST['vencimiento']);
	$vencimiento 	= date('Y-m-d',strtotime($vc));

	$sql="UPDATE ".MAIN_DB_PREFIX."repartos_vehiculos
			SET status='".$status."', idvehiculo='".$idv."', auto='".$auto."', modelo='".$modelo."', 
			    placas='".$placas."', secompro='".$secompro."', capacidad='".$capacidad."', aseguradora='".$aseguradora."',
			    telefono='".$telefono."', numpoliza='".$numpoliza."', vencimiento='".$vencimiento."'
			WHERE entity=".$conf->entity." AND rowid=".$idvei;
	if( $rs = $db->query($sql) ) {
		print "<script>window.location.href='card.php?id=".$idvei."';</script>";
	}
}

if( $action == '' && $idvei == '' ) {

	//print_fiche_titre('Nuevo Veh&iacute;culo','','setup');
	$linkback="";
    print load_fiche_titre("Nuevo veh&iacute;culo",$linkback,'title_companies.png');

	print "<form method='POSt' action='card.php'>";
	print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
	dol_fiche_head(null, 'card', '', 0, '');
	print "<table class='border' width='100%'>";
		print "<input type='hidden' name='action' value='add'>";
		print "<tr>";
			print "<td width='30%'>Estatus</td>";
			print "<td><select name='status'  >
						<option value='0'> </option>
						<option value='1'>Activo</option>
						<option value='2'>Inactivo</option></select></td>";
		print "</tr>";
		print "<tr>";
			print "<td>ID</td>";
			print "<td><input type='text' name='idv' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Auto</td>";
			print "<td><input type='text' name='auto' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Modelo</td>";
			print "<td><input type='text' name='modelo' size='30' ></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Placas</td>";
			print "<td><input type='text' name='placas' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Se compro</td>";
			print "<td>";
			$form->select_date($dateop,'secompro',0,0,0,'transaction');
			print "</td>";
		print "</tr>";
		print "<tr>";
		print "<td>Capacidad  de carga</td>";
		print "<td><input type='text' name='capacidad' size='30'></td>";
		print "</tr>";
		print "<tr class='liste_titre'>";
		print "<td colspan='2'>Datos de seguro</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Aseguradora</td>";
			print "<td><input type='text' name='aseguradora' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Telefono</td>";
			print "<td><input type='text' name='telefono' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>N&uacute;mero de P&oacute;liza</td>";
			print "<td><input type='text' name='numpoliza' size='30'></td>";
		print "<tr>";
			print "<td>Fecha de vencimiento</td><td>";
			$form->select_date($dateop,'vencimiento',0,0,0,'transaction');
			print "</td></tr>";
		//print "<tr>";
			//print "<td colspan='2' align='center'><input type='submit' name='add' value='Crear'></td>";
		//print "</tr>";
	print "</table>";

	dol_fiche_end();

	print '<div class="center">';
	print '<input type="submit" class="button" name="add" value="Crear veh&iacute;culo">';
	if ($backtopage) {
	    print ' &nbsp; ';
	    //print '<input type="submit" class="button" name="cancel" value="'.$langs->trans('Cancel').'">';
	}
	print '</div>'."\n";
	print '</form>'."\n";
}

if( $action == '' && $idvei > 0 ) {

	$sql = "SELECT status, idvehiculo, auto, modelo, placas, secompro, capacidad, aseguradora, 
			telefono, numpoliza, vencimiento
		FROM ".MAIN_DB_PREFIX."repartos_vehiculos
		WHERE entity=".$conf->entity." AND rowid=".$idvei;
	$rs = $db->query($sql);
	$rq = $db->fetch_object($rs);

	//print_fiche_titre('Veh&iacute;culo','','setup');
	$linkback="";
	print load_fiche_titre("Veh&iacute;culo",$linkback,'title_companies.png');
	dol_fiche_head(null, 'card', '', 0, '');

	print "<table class='border' width='100%'>";
		print "<tr>";
			print "<td width='30%'>Estatus</td>";
			$status="";
			if($rq->status==1){$status="Activo";}
			if($rq->status==2){$status="Baja";}
			print "<td>".$status."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>ID</td>";
			print "<td>".$rq->idvehiculo."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Auto</td>";
			print "<td>".$rq->auto."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Modelo</td>";
			print "<td>".$rq->modelo."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Placas</td>";
			print "<td>".$rq->placas."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Se compro</td>";
			print "<td>".date('d-m-Y',strtotime($rq->secompro))."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Capacidad  de carga</td>";
			print "<td>".$rq->capacidad."</td>";
		print "</tr>";
		print "<tr class='liste_titre'>";
			print "<td colspan='2'>Datos de seguro</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Aseguradora</td>";
			print "<td>".$rq->aseguradora."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Telefono</td>";
			print "<td>".$rq->telefono."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>N&uacute;mero de P&oacute;liza</td>";
			print "<td>".$rq->numpoliza."</td>";
		print "<tr>";
			print "<td>Fecha de vencimiento</td>";
			print "<td>".date('d-m-Y',strtotime($rq->vencimiento))."</td>";
		print "</tr>";
	print "</table>";
	//print '<div class="tabsAction">'."\n";
		//print '<div class="inline-block divButAction"><a class="butAction" href="card.php?id='.$idvei.'&action=edit">Modificar</a></div>';
	//print "</div>";
	dol_fiche_end();

	print '<div class="tabsAction">'."\n";
    print '<div class="inline-block divButAction"><a class="butAction" href="card.php?id='.$idvei.'&action=edit">'.$langs->trans("Modify").'</a></div>'."\n";
    print '<div class="inline-block divButAction"><span id="action-delete" class="butActionDelete">'.$langs->trans('Delete').'</span></div>'."\n";
    if ($backtopage)
    {
        print ' &nbsp; ';
        //print '<input type="submit" class="button" name="cancel" value="'.$langs->trans('Cancel').'">';
    }
    print '</div>'."\n";

}

if( $action == 'edit' && $idvei > 0 ) {

	$sql = "SELECT status, idvehiculo, auto, modelo, placas, secompro, capacidad, aseguradora,
			telefono, numpoliza, vencimiento
		FROM ".MAIN_DB_PREFIX."repartos_vehiculos
		WHERE entity=".$conf->entity." AND rowid=".$idvei;
	$rs = $db->query($sql);
	$rq = $db->fetch_object($rs);

	//print_fiche_titre('Nuevo Veh&iacute;culo','','setup');
	$linkback="";
	print load_fiche_titre("Conductor",$linkback,'title_companies.png');

	print "<form method='POSt' action='card.php?id=".$idvei."'>";
	
	dol_fiche_head(null, 'card', '', 0, '');

	print "<table class='border' width='100%'>";
		print "<input type='hidden' name='action' value='confedit'>";
		print "<tr>";
			print "<td width='30%'>Estatus</td>";
			$a='';$b='';$c='';
			if($rq->status==0){$a=' SELECTED';}
			if($rq->status==1){$b=' SELECTED';}
			if($rq->status==2){$c=' SELECTED';}
			print "<td><select name='status'  >
								<option value='0' ".$a."> </option>
								<option value='1' ".$b.">Activo</option>
								<option value='2' ".$c.">Inactivo</option></select></td>";
		print "</tr>";
		print "<tr>";
		print "<td>ID</td>";
		print "<td><input type='text' name='idv' size='30' value='".$rq->idvehiculo."'></td>";
		print "</tr>";
		print "<tr>";
		print "<td>Auto</td>";
		print "<td><input type='text' name='auto' size='30' value='".$rq->auto."'></td>";
		print "</tr>";
		print "<tr>";
		print "<td>Modelo</td>";
		print "<td><input type='text' name='modelo' size='30' value='".$rq->modelo."'></td>";
		print "</tr>";
		print "<tr>";
		print "<td>Placas</td>";
		print "<td><input type='text' name='placas' size='30' value='".$rq->placas."'></td>";
		print "</tr>";
		print "<tr>";
		print "<td>Se compro</td>";
		print "<td>";
		$form->select_date(strtotime($rq->secompro),'secompro',0,0,0,'transaction');
		print "</td>";
		print "</tr>";
		print "<tr>";
		print "<td>Capacidad  de carga</td>";
		print "<td><input type='text' name='capacidad' size='30' value='".$rq->capacidad."'></td>";
		print "</tr>";
		print "<tr class='liste_titre'>";
		print "<td colspan='2'>Datos de seguro</td>";
		print "</tr>";
		print "<tr>";
		print "<td>Aseguradora</td>";
		print "<td><input type='text' name='aseguradora' size='30' value='".$rq->aseguradora."'></td>";
		print "</tr>";
		print "<tr>";
		print "<td>Telefono</td>";
		print "<td><input type='text' name='telefono' size='30' value='".$rq->telefono."'></td>";
		print "</tr>";
		print "<tr>";
		print "<td>N&uacute;mero de P&oacute;liza</td>";
		print "<td><input type='text' name='numpoliza' size='30' value='".$rq->numpoliza."'></td>";
		print "<tr>";
		print "<td>Fecha de vencimiento</td><td>";
		$form->select_date(strtotime($rq->vencimiento),'vencimiento',0,0,0,'transaction');
		print "</td></tr>";
		//print "<tr>";
		//print "<td colspan='2' align='center'><input type='submit' name='edit' value='Actualizar'></td>";
		//print "</tr>";
	print "</table>";
	dol_fiche_end();

	print '<div class="center">';
	print '<input type="submit" class="button" name="edit" value="'.$langs->trans("Save").'">';
	print '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    print '<input type="submit" class="button" name="cancel" value="'.$langs->trans("Cancel").'">';
	if ($backtopage) {
	    print ' &nbsp; ';
	    //print '<input type="submit" class="button" name="cancel" value="'.$langs->trans('Cancel').'">';
	}
	print '</div>'."\n";
	print "</form>";
	
}