<?php
/* Copyright (C) 2007-2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *   	\file       repartos/repartosvehiculos_card.php
 *		\ingroup    repartos
 *		\brief      This file is an example of a php page
 *					Initialy built by build_class_from_table on 2016-12-21 18:26
 */

//if (! defined('NOREQUIREUSER'))  define('NOREQUIREUSER','1');
//if (! defined('NOREQUIREDB'))    define('NOREQUIREDB','1');
//if (! defined('NOREQUIRESOC'))   define('NOREQUIRESOC','1');
//if (! defined('NOREQUIRETRAN'))  define('NOREQUIRETRAN','1');
//if (! defined('NOCSRFCHECK'))    define('NOCSRFCHECK','1');			// Do not check anti CSRF attack test
//if (! defined('NOSTYLECHECK'))   define('NOSTYLECHECK','1');			// Do not check style html tag into posted data
//if (! defined('NOTOKENRENEWAL')) define('NOTOKENRENEWAL','1');		// Do not check anti POST attack test
//if (! defined('NOREQUIREMENU'))  define('NOREQUIREMENU','1');			// If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))  define('NOREQUIREHTML','1');			// If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))  define('NOREQUIREAJAX','1');
//if (! defined("NOLOGIN"))        define("NOLOGIN",'1');				// If this page is public (can be called outside logged session)

// Change this following line to use the correct relative path (../, ../../, etc)
$res=0;
if (! $res && file_exists("../main.inc.php")) $res=@include '../main.inc.php';					// to work if your module directory is into dolibarr root htdocs directory
if (! $res && file_exists("../../main.inc.php")) $res=@include '../../main.inc.php';			// to work if your module directory is into a subdir of root htdocs directory
if (! $res && file_exists("../../../dolibarr/htdocs/main.inc.php")) $res=@include '../../../dolibarr/htdocs/main.inc.php';     // Used on dev env only
if (! $res && file_exists("../../../../dolibarr/htdocs/main.inc.php")) $res=@include '../../../../dolibarr/htdocs/main.inc.php';   // Used on dev env only
if (! $res) die("Include of main fails");
// Change this following line to use the correct relative path from htdocs
include_once(DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php');
dol_include_once('/repartos/class/repartosvehiculos.class.php');

// Load traductions files requiredby by page

$langs->load("other");
$langs->load("repartos@repartos");

// Get parameters
$id			= GETPOST('id','int');
$action		= GETPOST('action','alpha');
$backtopage = GETPOST('backtopage');
$myparam	= GETPOST('myparam','alpha');


$search_entity=GETPOST('search_entity','int');
$search_status=GETPOST('search_status','int');
$search_idvehiculo=GETPOST('search_idvehiculo','alpha');
$search_auto=GETPOST('search_auto','alpha');
$search_modelo=GETPOST('search_modelo','alpha');
$search_placas=GETPOST('search_placas','alpha');
$search_capacidad=GETPOST('search_capacidad','alpha');
$search_aseguradora=GETPOST('search_aseguradora','alpha');
$search_telefono=GETPOST('search_telefono','alpha');
$search_numpoliza=GETPOST('search_numpoliza','alpha');



// Protection if external user
if ($user->societe_id > 0)
{
	//accessforbidden();
}

if (empty($action) && empty($id) && empty($ref)) $action='list';

// Load object if id or ref is provided as parameter
$object=new Repartosvehiculos($db);
if (($id > 0 || ! empty($ref)) && $action != 'add')
{
	$result=$object->fetch($id,$ref);
	if ($result < 0) dol_print_error($db);
}

// Initialize technical object to manage hooks of modules. Note that conf->hooks_modules contains array array
$hookmanager->initHooks(array('repartosvehiculos'));
$extrafields = new ExtraFields($db);



/*******************************************************************
* ACTIONS
*
* Put here all code to do according to value of "action" parameter
********************************************************************/

$parameters=array();
$reshook=$hookmanager->executeHooks('doActions',$parameters,$object,$action);    // Note that $action and $object may have been modified by some hooks
if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

if (empty($reshook))
{
	// Action to add record
	if ($action == 'add')
	{
		if (GETPOST('cancel'))
		{
			$urltogo=$backtopage?$backtopage:dol_buildpath('/repartos/vehiculo/repartosvehiculos_list.php',1);
			header("Location: ".$urltogo);
			exit;
		}

		$error=0;

		/* object_prop_getpost_prop */
		
	$object->entity=$conf->entity;
	$object->status=GETPOST('status','int');
	$object->idvehiculo=GETPOST('idvehiculo','alpha');
	$object->auto=GETPOST('auto','alpha');
	$object->modelo=GETPOST('modelo','alpha');
	$object->placas=GETPOST('placas','alpha');
	$object->capacidad=GETPOST('capacidad','alpha');
	$object->aseguradora=GETPOST('aseguradora','alpha');
	$object->telefono=GETPOST('telefono','alpha');
	$object->numpoliza=GETPOST('numpoliza','alpha');

	$object->secompro     =dol_mktime(0,0,0,GETPOST("date_compromonth"),GETPOST("date_comproday"),GETPOST("date_comproyear"));
	$object->vencimiento   =dol_mktime(0,0,0,GETPOST("date_vencimientomonth"),GETPOST("date_vencimientoday"),GETPOST("date_vencimientoyear"));


		if (empty($object->status))
		{
			$error++;
			setEventMessages($langs->trans("ErrorFieldRequired",$langs->trans("rep_status")), null, 'errors');
		}

		if (empty($object->idvehiculo))
		{
			$error++;
			setEventMessages($langs->trans("ErrorFieldRequired",$langs->trans("rep_idvehiculo")), null, 'errors');
		}

		if (! $error)
		{
			$result=$object->create($user);
			if ($result > 0)
			{
				// Creation OK
				$urltogo=$backtopage?$backtopage:dol_buildpath('/repartos/vehiculo/repartosvehiculos_list.php',1);
				header("Location: ".$urltogo);
				exit;
			}
			{
				// Creation KO
				if (! empty($object->errors)) setEventMessages(null, $object->errors, 'errors');
				else  setEventMessages($object->error, null, 'errors');
				$action='create';
			}
		}
		else
		{
			$action='create';
		}
	}

	// Cancel
	if ($action == 'update' && GETPOST('cancel')) $action='view';

	// Action to update record
	if ($action == 'update' && ! GETPOST('cancel'))
	{
		$error=0;

		
	$object->entity=GETPOST('entity','int');
	$object->status=GETPOST('status','int');
	$object->idvehiculo=GETPOST('idvehiculo','alpha');
	$object->auto=GETPOST('auto','alpha');
	$object->modelo=GETPOST('modelo','alpha');
	$object->placas=GETPOST('placas','alpha');
	$object->capacidad=GETPOST('capacidad','alpha');
	$object->aseguradora=GETPOST('aseguradora','alpha');
	$object->telefono=GETPOST('telefono','alpha');
	$object->numpoliza=GETPOST('numpoliza','alpha');
	
	$object->secompro     =dol_mktime(0,0,0,GETPOST("date_compromonth"),GETPOST("date_comproday"),GETPOST("date_comproyear"));
	$object->vencimiento   =dol_mktime(0,0,0,GETPOST("date_vencimientomonth"),GETPOST("date_vencimientoday"),GETPOST("date_vencimientoyear"));		

		if (empty($object->status))
		{
			$error++;
			setEventMessages($langs->trans("ErrorFieldRequired",$langs->trans("rep_status")), null, 'errors');
		}

		if (empty($object->idvehiculo))
		{
			$error++;
			setEventMessages($langs->trans("ErrorFieldRequired",$langs->trans("rep_idvehiculo")), null, 'errors');
		}

		if (! $error)
		{
			$result=$object->update($user);
			if ($result > 0)
			{
				$action='view';
			}
			else
			{
				// Creation KO
				if (! empty($object->errors)) setEventMessages(null, $object->errors, 'errors');
				else setEventMessages($object->error, null, 'errors');
				$action='edit';
			}
		}
		else
		{
			$action='edit';
		}
	}

	// Action to delete
	if ($action == 'confirm_delete')
	{
		$result=$object->delete($user);
		if ($result > 0)
		{
			// Delete OK
			setEventMessages("RecordDeleted", null, 'mesgs');
			header("Location: ".dol_buildpath('/repartos/vehiculo/repartosvehiculos_list.php',1));
			exit;
		}
		else
		{
			if (! empty($object->errors)) setEventMessages(null, $object->errors, 'errors');
			else setEventMessages($object->error, null, 'errors');
		}
	}
}




/***************************************************
* VIEW
*
* Put here all code to build page
****************************************************/


llxHeader('',$langs->trans('rep_titre'),'');
$form=new Form($db);


// Put here content of your page

// Example : Adding jquery code
print '<script type="text/javascript" language="javascript">
jQuery(document).ready(function() {
	function init_myfunc()
	{
		jQuery("#myid").removeAttr(\'disabled\');
		jQuery("#myid").attr(\'disabled\',\'disabled\');
	}
	init_myfunc();
	jQuery("#mybutton").click(function() {
		init_myfunc();
	});
});
</script>';


// Part to create
if ($action == 'create')
{
	print load_fiche_titre($langs->trans("rep_create_vehiculo"));

	print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
	print '<input type="hidden" name="action" value="add">';
	print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';

	dol_fiche_head();

	print '<table class="border centpercent">'."\n";
	// print '<tr><td class="fieldrequired">'.$langs->trans("Label").'</td><td><input class="flat" type="text" size="36" name="label" value="'.$label.'"></td></tr>';
	// 
//print '<tr><td class="fieldrequired">'.$langs->trans("Fieldentity").'</td><td><input class="flat" type="text" name="entity" value="'.GETPOST('entity').'"></td></tr>';
//print '<tr><td class="fieldrequired">'.$langs->trans("rep_status").'</td><td><input class="flat" type="text" name="status" value="'.GETPOST('status').'"></td></tr>';
		print '<tr ><td class="fieldrequired" style="width: 15%;">'.$langs->trans("rep_status").'</td>';
		print '<td>';
		print '<select name="status" class="flat">';
			print '<option value="0"></option>';
			print '<option value="1" '.(GETPOST("status") == 1 ? "selected": "").'>Activo</option>';
			print '<option value="2" '.(GETPOST("status") == 2 ? "selected": "").'>Inactivo</option>';			
		print '</select>';
		
		print '</td></tr>';
		print '<tr><td class="fieldrequired">'.$langs->trans("rep_idvehiculo").'</td><td><input class="flat" type="text" name="idvehiculo" value="'.GETPOST('idvehiculo').'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_auto").'</td><td><input class="flat" type="text" name="auto" value="'.GETPOST('auto').'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_modelo").'</td><td><input class="flat" type="text" name="modelo" value="'.GETPOST('modelo').'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_placas").'</td><td><input class="flat" type="text" name="placas" value="'.GETPOST('placas').'"></td></tr>';

		print "<tr>";
		print "<td>Se compró</td>";
			print "<td>";
			 print ($form->select_date($object->secompro,'date_compro' ,0, 0, 0, "", 1, 1, 1, 0, '', '', ''));
			print "</td>";
		print "</tr>";

		print '<tr><td class="">'.$langs->trans("rep_capacidad").'</td><td><input class="flat" type="text" name="capacidad" value="'.GETPOST('capacidad').'"></td></tr>';

		print "<tr class='liste_titre'  style='height: 15%;  border-spacing:  50px;'>";
			print "<td colspan='2'>Datos del seguro</td>";
		print "</tr>";

		print '<tr><td class="">'.$langs->trans("rep_segure").'</td><td><input class="flat" type="text" name="aseguradora" value="'.GETPOST('aseguradora').'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_phone").'</td><td><input class="flat" type="text" name="telefono" value="'.GETPOST('telefono').'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_numPoli").'</td><td><input class="flat" type="text" name="numpoliza" value="'.GETPOST('numpoliza').'"></td></tr>';
				

		print "<tr>";
			print "<td>Fecha de vencimiento</td><td>";
				print ($form->select_date($object->vencimiento,'date_vencimiento' ,0, 0, 0, "", 1, 1, 1, 0, '', '', ''));
		print "</td></tr>";
		print "<tr>";


	print '</table>'."\n";

	dol_fiche_end();

	print '<div class="center"><input type="submit" class="button" name="add" value="'.$langs->trans("Create").'"> &nbsp; <input type="submit" class="button" name="cancel" value="'.$langs->trans("Cancel").'"></div>';

	print '</form>';
}



// Part to edit record
if (($id || $ref) && $action == 'edit')
{
	print load_fiche_titre($langs->trans("rep_edit_vehiculo"));
    
	print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
	print '<input type="hidden" name="action" value="update">';
	print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
	print '<input type="hidden" name="id" value="'.$object->id.'">';
	
	dol_fiche_head();

	print '<table class="border centpercent">'."\n";
	// print '<tr><td class="fieldrequired">'.$langs->trans("Label").'</td><td><input class="flat" type="text" size="36" name="label" value="'.$label.'"></td></tr>';
	// 
		//print '<tr><td class="fieldrequired">'.$langs->trans("Fieldentity").'</td><td><input class="flat" type="text" name="entity" value="'.$object->entity.'"></td></tr>';
		print '<tr ><td class="fieldrequired" style="width: 15%;">'.$langs->trans("rep_status").'</td>';
			print '<td>';
			print '<select name="status" class="flat">';
				print '<option value="0"></option>';
				print '<option value="1" '.($object->status == 1 ? "selected": "").'>Activo</option>';
				print '<option value="2" '.($object->status == 2 ? "selected": "").'>Inactivo</option>';	
		print '</td></tr>';

		print '<tr><td class="fieldrequired">'.$langs->trans("rep_idvehiculo").'</td><td><input class="flat" type="text" name="idvehiculo" value="'.$object->idvehiculo.'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_auto").'</td><td><input class="flat" type="text" name="auto" value="'.$object->auto.'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_modelo").'</td><td><input class="flat" type="text" name="modelo" value="'.$object->modelo.'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_placas").'</td><td><input class="flat" type="text" name="placas" value="'.$object->placas.'"></td></tr>';

		print "<tr>";
			print "<td>Se compró</td>";
			print "<td>";
			 print ($form->select_date($object->secompro,'date_compro' ,0, 0, 0, "", 1, 1, 1, 0, '', '', ''));
			print "</td>";
		print "</tr>";

		print '<tr><td class="">'.$langs->trans("rep_capacidad").'</td><td><input class="flat" type="text" name="capacidad" value="'.$object->capacidad.'"></td></tr>';

		print "<tr class='liste_titre'  style='height: 15%;  border-spacing:  50px;'>";
			print "<td colspan='2'>Datos de seguro</td>";
		print "</tr>";

		print '<tr><td class="">'.$langs->trans("rep_segure").'</td><td><input class="flat" type="text" name="aseguradora" value="'.$object->aseguradora.'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_phone").'</td><td><input class="flat" type="text" name="telefono" value="'.$object->telefono.'"></td></tr>';
		print '<tr><td class="">'.$langs->trans("rep_numPoli").'</td><td><input class="flat" type="text" name="numpoliza" value="'.$object->numpoliza.'"></td></tr>';
		print "<tr>";
			print "<td>Fecha de vencimiento</td>";
			print "<td>";
			 print ($form->select_date($object->vencimiento,'date_vencimiento' ,0, 0, 0, "", 1, 1, 1, 0, '', '', ''));
			print "</td>";
		print "</tr>";

	print '</table>';
	
	dol_fiche_end();

	print '<div class="center"><input type="submit" class="button" name="save" value="'.$langs->trans("Save").'">';
	print ' &nbsp; <input type="submit" class="button" name="cancel" value="'.$langs->trans("Cancel").'">';
	print '</div>';

	print '</form>';
}



// Part to show record
if ($id && (empty($action) || $action == 'view' || $action == 'delete'))
{
	print load_fiche_titre($langs->trans("rep_vehi"));
    
	dol_fiche_head();

	if ($action == 'delete') {
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $object->id, $langs->trans('rep_titre_msgVehi_delete'), $langs->trans('rep_msgVehi_delete'), 'confirm_delete', '', 0, 1);
		print $formconfirm;
	}
	
	print '<table class="border centpercent">'."\n";
	// print '<tr><td class="fieldrequired">'.$langs->trans("Label").'</td><td><input class="flat" type="text" size="36" name="label" value="'.$label.'"></td></tr>';
	// 
//print '<tr><td class="fieldrequired">'.$langs->trans("Fieldentity").'</td><td>$object->entity</td></tr>';	
	print '<tr><td class=""  style="width: 15%;">'.$langs->trans("rep_status").'</td><td>'.$langs->trans('rep_status'.$object->status).'</td></tr>';
	print '<tr><td class="fieldrequired">'.$langs->trans("rep_idvehiculo").'</td><td>'.$object->idvehiculo.'</td></tr>';
	print '<tr><td class="">'.$langs->trans("rep_auto").'</td><td>'.$object->auto.'</td></tr>';
	print '<tr><td class="">'.$langs->trans("rep_modelo").'</td><td>'.$object->modelo.'</td></tr>';
	print '<tr><td class="">'.$langs->trans("rep_placas").'</td><td>'.$object->placas.'</td></tr>';

	print '<tr><td>Ingreso</td><td>';
			print ($form->select_date($object->secompro,'date_compro' ,0, 0, 0, "", 1, 0, 0, 1, '', '', ''));
	print "</td></tr>";

	print '<tr><td class="">'.$langs->trans("rep_capacidad").'</td><td>'.$object->capacidad.'</td></tr>';

	print "<tr class='liste_titre'  style='height: 15%;  border-spacing:  50px;'>";
		print "<td colspan='2'>Datos del seguro</td>";
	print "</tr>";

	print '<tr><td class="">'.$langs->trans("rep_segure").'</td><td>'.$object->aseguradora.'</td></tr>';
	print '<tr><td class="">'.$langs->trans("rep_phone").'</td><td>'.$object->telefono.'</td></tr>';
	print '<tr><td class="">'.$langs->trans("rep_numPoli").'</td><td>'.$object->numpoliza.'</td></tr>';

	print '<tr><td>Ingreso</td><td>';
			print ($form->select_date($object->vencimiento,'date_vencimiento' ,0, 0, 0, "", 1, 0, 0, 1, '', '', ''));
	print "</td></tr>";

	print '</table>';
	
	dol_fiche_end();


	// Buttons
	print '<div class="tabsAction">'."\n";
	$parameters=array();
	$reshook=$hookmanager->executeHooks('addMoreActionsButtons',$parameters,$object,$action);    // Note that $action and $object may have been modified by hook
	if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

	if (empty($reshook))
	{
		if ($user->rights->repartos->write_vehic)
		{
			print '<div class="inline-block divButAction"><a class="butAction" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&amp;action=edit">'.$langs->trans("Modify").'</a></div>'."\n";
		}

		if ($user->rights->repartos->delete_vehic)
		{
			print '<div class="inline-block divButAction"><a class="butActionDelete" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&amp;action=delete">'.$langs->trans('Delete').'</a></div>'."\n";
		}
	}
	print '</div>'."\n";


	// Example 2 : Adding links to objects
	//$somethingshown=$form->showLinkedObjectBlock($object);
	//$linktoelem = $form->showLinkToObjectBlock($object);
	//if ($linktoelem) print '<br>'.$linktoelem;

}


// End of page
llxFooter();
$db->close();
