function calculoRestante(lmnt) {
    var y = $("#" + lmnt.id + " #cantEstimada")[0].innerHTML;
    var x = $("#" + lmnt.id + " #cantEntregada")[0].value;
    $("#" + lmnt.id + " #cantRestante")[0].innerHTML = 'Cantidad Restante: ' + (Number(y) - Number(x));
}

$(function () {
    $('#addDelivery').on('submit', function (e) {
        var entregas = $(this).serializeArray();
        //if (confirm("Agregar dialogo?")) {
        $.ajax({
            type: 'post',
            async: false,
            url: 'repartos_entregas.php?action=addDeliveries',
            data: entregas,
            success: function (data) {
                window.location.href = 'repartosgestion_list.php';
            }
        });
        //}
        e.preventDefault();
    });
});

$(function () {
    $('#addF').on('submit', function (e) {
        var datos = $(this).serializeArray();
        var reenvioFacturas = "false";
        var check = false;
        if(datos.length >= 4 && datos[1]['value'] != "" && datos[2]['value'] != ""){
            $.ajax({
                type: 'post',
                async: false,
                url: 'checkdeliveries.php?action=checkFactures',
                data: datos,
                success: function (data) {
                    reenvioFacturas = data;
                }
            });
        } else {
            var check = true;
        }
        if (reenvioFacturas === "false") { //si no es un reenvio
            $.ajax({
                type: 'post',
                async: false,
                url: 'card.php',
                data: datos
            });
            e.preventDefault();
            if(!check){
                window.location.href = 'repartosgestion_list.php';
            }
        }
        else { // si se trata de una factura con reenvio
            $(document.documentElement).append(
                `<div id="dialogpass" title="Contraseña Supervisor">
                <form id='Spass' action="javascript:checkpass();">
                    Supervisor: </br>
                    <input name='pass' id='Spassinput' type="password" size="25"/>
                    <input type="submit" value="Verificar">
                </form>
                </div>`
            );
            $( "#dialogpass" ).dialog();
            e.preventDefault();
        }
        //e.preventDefault();
    });
});


function checkpass() {
    var datos = $('#Spass').serializeArray();
    $.ajax({
        type: 'post',
        async: false,
        url: 'checkdeliveries.php?action=checkCorrectPass',
        data: datos,
        success: function (data) {
            if(data === "true"){
                continuarBorrador();
            } else {
                document.getElementById('Spassinput').style.borderColor = "red";
                document.getElementById('Spassinput').style.borderWidth = "thick";
            }
        }
    });
}


function continuarBorrador() {
    var datos = $('#addF').serializeArray();
    $.ajax({
        type: 'post',
        async: false,
        url: 'card.php',
        data: datos
    });
    $( "#dialogpass" ).dialog('destroy').remove();
    window.location.href = 'repartosgestion_list.php';
}
