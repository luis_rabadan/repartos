<?php
$res=@include("../../main.inc.php");                                // For root directory
if (! $res) $res=include("../../../main.inc.php"); 

// Protection if external user
if ($user->societe_id > 0)
{
	//accessforbidden();
}

// Get parameters
$id			= GETPOST('id','int');
$action		= GETPOST('action','alpha');
$backtopage = GETPOST('backtopage');
$myparam	= GETPOST('myparam','alpha');
// Extras
$vehi 	= GETPOST('vehiculo');
$conduc = GETPOST('conductor');
$ckfactura = GETPOST('ckfactura');

if($action == 'checkFactures') {
    $ReenvioFactura = false;
	$data_array = array();
	$sqlcheck = "
	SELECT 
		a.`fk_facture` 
	FROM   `llx_repartos_gestion_facture` a, 
		`llx_repartos_gestion` b 
	WHERE  a.`fk_gestion` = b.`rowid` 
		AND b.`status` = 2 
	";
	$rq = $db->query($sqlcheck);
	while( $rs = $db->fetch_object($rq) ){
		$data_array[] = $rs->fk_facture;
	}
	//print_r($_POST['ckfactura']);
	//print_r($data_array);
	foreach ($_POST['ckfactura'] as &$valor) {
		foreach ($data_array as &$value) {
			//echo $valor." ".$value;
			if ($valor == $value){
				$ReenvioFactura = true;
			}
		}
	}
    echo json_encode($ReenvioFactura);
}

if($action == 'checkCorrectPass') {
	$correctpass = false;
	$huser=new User($db);
	$huser->fetch($user->fk_user);
	if($user->admin == 1){
		$pass = $user->pass_indatabase_crypted;
	}
	else {
		$pass = $huser->pass_indatabase_crypted;
	}
	if ( md5(GETPOST('pass')) == $pass ){
		$correctpass = true;
	}
	echo json_encode($correctpass);
}