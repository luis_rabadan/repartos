<?php
	require_once("../lib/mpdf/mpdf.php");
	$res=@include("../../main.inc.php");                                // For root directory
	if (! $res) $res=include("../../../main.inc.php");  

	require_once(DOL_DOCUMENT_ROOT."/core/class/html.formfile.class.php");

	$id=$_GET['id'];

	$sql = "SELECT";
	$sql .= " t.fk_vehiculo,";
	$sql .= " t.fk_conductor,";
	$sql .= " t.rowid,";
	$sql .= " t.status,";
	$sql .= " concat(t2.idvehiculo,' - ',t2.auto, ' - ', t2.modelo) as nomVehi,";
	$sql .= " concat(t3.idconductor,' - ',t3.nombre) as nomCond,";
	$sql .= " t2.auto,";
	$sql .= " t2.idvehiculo,";
	$sql .= " t2.modelo,";
	$sql .= " t2.capacidad,";
	$sql .= " t3.idconductor,";
	$sql .= " t3.nombre";

	$sql.= " FROM ".MAIN_DB_PREFIX."repartos_gestion as t INNER JOIN ".MAIN_DB_PREFIX."repartos_vehiculos t2 on  t.fk_vehiculo=t2.rowid INNER JOIN ".MAIN_DB_PREFIX."repartos_conductores t3 on t.fk_conductor=t3.rowid";	
	$sql.= " WHERE t.entity=".$conf->entity." AND t.rowid=".$id;
	
	$rql=$db->query($sql);
	$rs=$db->fetch_object($rql);


	$sql2="SELECT a.qty, b.facnumber, c.ref, c.label, c.weight
		FROM ".MAIN_DB_PREFIX."repartos_gestion_facturedet a, ".MAIN_DB_PREFIX."facture b, ".MAIN_DB_PREFIX."product c
		WHERE a.fk_gestion=".$id." AND a.fk_facture=b.rowid AND a.fk_product=c.rowid";
	$rq=$db->query($sql2);
			

	
	$pdf= new mPDF('c','A4','','',20,20,13.5,10);

	ob_start();
  		echo "<page>";
		?>
		<div align='center'><b>Gesti&oacute;n de tr&aacute;fico</b></div><br/><br/>

		<table  align='left' width="80%" >
			<tr>
				<td><b>Veh&iacute;culo:</b></td>
			 	<td>			
				 	<?php echo $rs->nomVehi?>
			 	</td>			 	
			 	<td><b>Capacidad:</b></td>
			 	<td>			
				 	<?php echo $rs->capacidad ?>
			 	</td>
			 </tr>
			 <tr>
			 	<td><b>Conductor:</b></td>
			 	<td>			
				 	<?php echo $rs->nomCond?>
			 	</td>
			 </tr>	
			 <tr>
			 	<td><b>Orden de tr&aacute;fico:</b></td>
			 	<td>			
				 	<?php echo $rs->rowid?>
			 	</td>
			 </tr>		
		 	
		 </table>
		 <br/><br/>
		 <table  align='left' width="100%" border="1" >
			<tr>
				<td><b>Factura</b></td>
			 	<td><b>Producto</b></td>	
			 	<td><b>Cantidad</b></td>
			 	<td><b>Peso Total</b></td>			 	
			</tr>
			<?php 
			
			
			$pesoglobal=0;
			while($rs=$db->fetch_object($rq)){
				
				$pesoglobal += number_format($rs->weight*$rs->qty,2,'.','');
				echo "<tr>";
					echo "<td>";
						echo $rs->facnumber;						
					echo "</td>";
					echo "<td>";
						echo $rs->ref." - ".$rs->label;
					echo "</td>";
					echo "<td align='right'>";
						echo $rs->qty;
					echo "</td>";
					echo "<td align='right'>";
						echo number_format($rs->weight*$rs->qty,2,'.','');						
					echo "</td>";
				echo "</tr>";
				}
			?>
			 <tr>
			 	<td colspan="2"></td>
			 	<td> <b>Peso Total</b></td>
			 	<td align='right'><b><?php echo number_format($pesoglobal,2,'.',''); ?></b></td>
			 	
			 </tr>			
		 	
		 </table>			

	<?php
	 echo "</page>";             
    $content = ob_get_clean();

	$pdf->SetHTMLHeader('
	<div>
		<p style="text-align: left; font-weight: bold;">Número de página: {PAGENO}</p>
  		<p style="text-align: right; font-weight: bold;">Fecha de elaboración: {DATE j-m-Y}</p>
	</div>
	');

	$pdf->SetHTMLFooter("
	<table width='100%' border='1' >
		<tr>
			<td align='center' valign='bottom' height='70px' width='33%'><b>Elaboró</b></td>
			<td align='center' valign='bottom' height='70px' width='33%'><b>Firma chofer</b></td>	
			<td align='center' valign='bottom' height='70px' width='33%'><b>Firma seguridad</b></td>			 	
		</tr>
	</table>
	");

	$pdf->writeHTML($content);

	$pdf->output('carga.php','I');
?>
	