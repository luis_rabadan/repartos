<?php
	require_once("../lib/mpdf/mpdf.php");
	$res=@include("../../main.inc.php");                                // For root directory
	if (! $res) $res=include("../../../main.inc.php");  

	require_once(DOL_DOCUMENT_ROOT."/core/class/html.formfile.class.php");

    $id=$_GET['id'];
	

	$sql = "SELECT";
		$sql.= " t.rowid,";
		$sql .= " t.facnumber,";
		$sql .= " t.entity,";		
		$sql .= " t.fk_soc,";		
		$sql .= " t.date_valid,";		
		$sql .= " t.paye,";		
		$sql .= " t.total_ttc,";
		$sql .= " t.fk_statut,";
		$sql .= " t2.name_alias as alias,";		
		$sql .= " t2.nom as client";	

	$sql.= " FROM ".MAIN_DB_PREFIX."facture as t INNER JOIN ".MAIN_DB_PREFIX."societe  as t2 on t2.rowid= t.fk_soc";	
	$sql.= " WHERE t.fk_statut = 1 AND t.paye = 0";
    $sql.= " AND t.rowid IN (SELECT b.fk_facture
			FROM ".MAIN_DB_PREFIX."repartos_gestion_facture b
			WHERE b.entity = t.entity AND b.fk_gestion = ".$id.")";
	
	$rq = $db->query($sql);

	$sql2 = "SELECT";
	$sql2 .= " t.fk_vehiculo,";
	$sql2 .= " t.fk_conductor,";
	$sql2 .= " t.rowid,";
	$sql2 .= " t.status,";
	$sql2 .= " concat(t2.idvehiculo,' - ',t2.auto, ' - ', t2.modelo) as nomVehi,";
	$sql2 .= " concat(t3.idconductor,' - ',t3.nombre) as nomCond,";
	$sql2 .= " t2.auto,";
	$sql2 .= " t2.idvehiculo,";
	$sql2 .= " t2.modelo,";
	$sql2 .= " t2.capacidad,";
	$sql2 .= " t3.idconductor,";
	$sql2 .= " t3.nombre";

	$sql2.= " FROM ".MAIN_DB_PREFIX."repartos_gestion as t INNER JOIN ".MAIN_DB_PREFIX."repartos_vehiculos t2 on  t.fk_vehiculo=t2.rowid INNER JOIN ".MAIN_DB_PREFIX."repartos_conductores t3 on t.fk_conductor=t3.rowid";	
	$sql2.= " WHERE t.entity=".$conf->entity." AND t.rowid=".$id;
	
	$rql=$db->query($sql2);
	$rs=$db->fetch_object($rql);
	
	$pdf= new mPDF('c','A4','','',20,20,13.5,10);

	ob_start();
  		echo "<page>";
		?>
		<div align='center'><b>Facturas por cobranza</b></div><br/><br/>
		<table  align='left' width="80%" >
			<tr>
				<td><b>Veh&iacute;culo:</b></td>
			 	<td>			
				 	<?php echo $rs->nomVehi?>
			 	</td>			 	
			 	<td><b>Capacidad:</b></td>
			 	<td>			
				 	<?php echo $rs->capacidad ?>
			 	</td>
			 </tr>
			 <tr>
			 	<td><b>Conductor:</b></td>
			 	<td>			
				 	<?php echo $rs->nomCond?>
			 	</td>
			 </tr>	
			 <tr>
			 	<td><b>Orden de tr&aacute;fico:</b></td>
			 	<td>			
				 	<?php echo $rs->rowid?>
			 	</td>
			 </tr>		
		 	
		 </table>
		 <br/><br/>
		 <table  align='left' width="100%" border="1" >
			<tr>
				<td><b>Documento</b></td>
			 	<td><b>Cliente</b></td>	
			 	<td><b>Fecha</b></td>
			 	<td><b>Estatus</b></td>
			 	<td><b>Por cobrar</b></td>
			 	<td><b>Firma cliente</b></td>			 	
			</tr>
			<?php 
			
			
			$total=0;
			while($rs = $db->fetch_object($rq)){
				
				$total+=number_format($rs->total_ttc,2,'.','');
				echo "<tr>";
					echo "<td>";
						echo $rs->facnumber;						
					echo "</td>";
					echo "<td>";
						echo $rs->client; $alias=(!empty($rs->alias))? ' ( '.$rs->alias.' )' : "" ; echo $alias;
					echo "</td>";
					echo "<td>";
						echo $rs->date_valid;
					echo "</td>";
					echo "<td>";
						echo $rs->paye;
					echo "</td>";
					echo "<td align='right'>";
						echo $rs->total_ttc;
					echo "</td>";
					echo "<td align='right'>";
						
					echo "</td>";
				echo "</tr>";
				}
			?>
			 <tr>
			 	<td colspan="3"></td>
			 	<td> <b>Total</b></td>
			 	<td align='right'><b><?php echo number_format($total,2,'.',''); ?></b></td>
			 	<td style="background-color:gray"></td>
			 </tr>			
		 	
		 </table>			

	<?php
	 echo "</page>";             
    $content = ob_get_clean();

	$pdf->SetHTMLHeader('
	<div>
		<p style="text-align: left; font-weight: bold;">Número de página: {PAGENO}</p>
  		<p style="text-align: right; font-weight: bold;">Fecha de elaboración: {DATE j-m-Y}</p>
	</div>
	');

	$pdf->writeHTML($content);
	
	$pdf->output('cobranzaIn.php','I');
?>
	