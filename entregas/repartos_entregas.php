<?php
$res=@include("../../main.inc.php");                                // For root directory
if (! $res) $res=include("../../../main.inc.php");  
dol_include_once('/repartos/class/repartosentregas.class.php');

// Protection if external user
if ($user->societe_id > 0)
{
	//accessforbidden();
}

// Get parameters
$id			= GETPOST('id','int');
$action		= GETPOST('action','alpha');
$backtopage = GETPOST('backtopage');
$myparam	= GETPOST('myparam','alpha');

//crear nuevas entregas
if ($action == 'addDeliveries'){
	//print "########Entro";
	$fk_gestion_facturedet = GETPOST('fk_gestion_facturedet');
	$fk_gestion = GETPOST('fk_gestion');
	$fk_facture = GETPOST('fk_facture');
	$qty = GETPOST('qty');
	$qtypr = GETPOST('qtypr');
	$comment = GETPOST('comment');

	//$date = GETPOST('date');
	$ok = false;
	for ($i=0; $i < sizeof($fk_gestion_facturedet); $i++) {
		echo "Iteracion: ".$i;
		$object = new Repartosentregas($db);
		$object->fk_gestion_facturedet = $fk_gestion_facturedet[$i];
		$object->qty = $qty[$i];
		$object->comment = $comment[$i];
		$object->date = date( "Y-m-d H:i:s", dol_mktime($_POST['datehour'], $_POST['datemin'] , 0, $_POST['datemonth'], $_POST['dateday'], $_POST['dateyear']) );
		$result = $object->create($user);
		echo "La hora y fecha: ".date( "Y-m-d H:i:s");
		echo "Fecha: ".$object->date;
		if ($result > 0){
			// Creation OK
			echo "Creado";
			$ok = true;
		}
		else {
			// Creation KO
			echo "No Creado";
			$ok = false;
		}
	}
	//agregar cambios cundo no fue entregado de manera completa
	for ($i=0; $i < sizeof($fk_gestion_facturedet); $i++) {
		// La gestion llego a la etapa de entrega
		$sql1 = "UPDATE llx_repartos_gestion SET status = 2 WHERE rowid=".$fk_gestion[$i];
		$db->query($sql1);

		// Actualizar la cantidad restante (por entregar)
		$sql2 = "UPDATE llx_repartos_gestion_facturedet SET qty=".$qty[$i]." WHERE rowid=".$fk_gestion_facturedet[$i];
		$db->query($sql2);

		// Verificar si la factura esta completada o no
		$sql3 = "SELECT a.fk_product, a.qty, ifnull(b.cant,0) as cant, (a.qty-ifnull(b.cant,0)) as resta
			FROM ".MAIN_DB_PREFIX."facturedet a 
					LEFT JOIN (SELECT sum(qty) as cant,fk_product 
					FROM ".MAIN_DB_PREFIX."repartos_gestion_facturedet WHERE fk_facture=".$fk_facture[$i]." 
				    GROUP BY fk_product) b ON a.fk_product=b.fk_product 
			WHERE a.fk_facture=".$fk_facture[$i]." AND product_type=0 AND a.fk_product>0";
		$rq = $db->query($sql3);
		$completa = 1;
		while ( $rs3 = $db->fetch_object($rq) ){
			if( $rs3->resta > 0 ) {
				$completa = 0;
			}
		}
		if( $completa == 1 ) {
			$sqp = "UPDATE llx_repartos_gestion_facture SET completa=1 WHERE fk_facture=".$fk_facture[$i];
			$rq3 = $db->query($sqp);
		}
		if( $completa == 0 ) {
			$sqp = "UPDATE llx_repartos_gestion_facture SET completa=0 WHERE fk_facture=".$fk_facture[$i];
			$rq3 = $db->query($sqp);
		}

	}
	//print_r( sizeof($data) );
	print_r($_POST);
	echo 'fin';
}
