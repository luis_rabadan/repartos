<?php
$res=@include("../../main.inc.php");                                // For root directory
if (! $res) $res=include("../../../main.inc.php");  

llxHeader("","Lista de Conductores",'');
print_fiche_titre('Lista de Conductores','','setup');


print "<table class='noborder' width='100%'>";
	print "<tr class='liste_titre'>";
		print "<td width='20%'>ID</td>";
		print "<td width='20%'>Estatus</td>";
		print "<td width='20%'>Nombre</td>";
		print "<td width='20%'>Teléfono</td>";
		print "<td width='20%'>Email</td>";
	print "</tr>";
	$sql="SELECT rowid, status, idconductor, nombre, direccion, telefono, email,
					depto, puesto, ingreso, vlicencia, cumpleanios
				FROM ".MAIN_DB_PREFIX."repartos_conductores
				WHERE entity=".$conf->entity." ORDER BY rowid DESC";
	$rs=$db->query($sql);
	
	while($rq=$db->fetch_object($rs)){
		print "<tr class='pair'>";
			print "<td><a href='nuevo.php?id=".$rq->rowid."'><strong>".$rq->idconductor.img_view()."</strong></a></td>";
			$status='';
			if($rq->status==1){$status='Activo';}
			if($rq->status==2){$status='Baja';}
			print "<td>".$status."</td>";
			print "<td>".$rq->nombre."</td>";
			print "<td>".$rq->telefono."</td>";
			print "<td>".$rq->email."</td>";
		print "</tr>";
	}

print "</table>";