<?php

$res = @include("../../main.inc.php");
if (! $res) $res=include("../../../main.inc.php");  

include_once(DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php');


llxHeader("","Conductor",'');

$action 	= GETPOST('action');
$idcon 		= GETPOST('id');
$cancel     = GETPOST('cancel');
$backtopage = GETPOST('backtopage','alpha');

if( $action == 'add' ) {

	$status			= GETPOST('status');
	$idc 			= GETPOST('idc');

	echo "id ".$idc;

	if (empty(GETPOST('idc')))
	{
		echo "aqui";
		setEventMessages($langs->trans("ErrorFieldRequired",$langs->transnoentitiesnoconv("ID")), null, 'errors');
	}

	$status			= GETPOST('status');
	$idc 			= GETPOST('idc');
	$nombre 		= GETPOST('nombre');
	$direc 			= GETPOST('direc');
	$tel 			= GETPOST('tel');
	$email 			= GETPOST('email');
	$depto 			= GETPOST('depto');
	$puesto 		= GETPOST('puesto');
	$ig 			= str_replace("/","-", $_POST['ingreso']);
	$ingreso 		= date('Y-m-d',strtotime($ig));
	$vl 			= str_replace("/","-", $_POST['vlicencia']);
	$vlicencia 		= date('Y-m-d',strtotime($vl));
	$cl 			= str_replace("/","-", $_POST['cumpleanios']);
	$cumpleanios 	= date('Y-m-d',strtotime($cl));

	$sql = "INSERT INTO ".MAIN_DB_PREFIX."repartos_conductores (entity, status, idconductor, nombre, direccion, 
				telefono, email, depto, puesto, ingreso, vlicencia, cumpleanios)
			VALUES('".$conf->entity."', '".$status."', '".$idc."', '".$nombre."', '".$direc."'
	, '".$tel."', '".$email."', '".$depto."', '".$puesto."', '".$ingreso."', '".$vlicencia."', '".$cumpleanios."')";
	//print $sql."<br>";
	if( $rs = $db->query($sql) ){
		$sql = "SELECT @@identity AS id";
		$rs = $db->query($sql);
		$rq = $db->fetch_object($rs);
		//print $rq->id;
		//print "<script>window.location.href='nuevo.php?id=".$rq->id."';</script>";
	}
	else {
		//dol_htmloutput_mesg('');
		dol_htmloutput_errors('Error al registrar el conductor');
	}
}

// Cancel
if ($action == 'confedit' && GETPOST('cancel')){
	print "<script>window.location.href='nuevo.php?id=".$idcon."';</script>";
}


if( $action == 'confedit' && ! GETPOST('cancel')) {
	
	$status 		= GETPOST('status');
	$idc 			= GETPOST('idc');
	$nombre 		= GETPOST('nombre');
	$direc 			= GETPOST('direc');
	$tel 			= GETPOST('tel');
	$email 			= GETPOST('email');
	$depto 			= GETPOST('depto');
	$puesto 		= GETPOST('puesto');
	$ig 			= str_replace("/","-", $_POST['ingreso']);
	$ingreso 		= date('Y-m-d',strtotime($ig));
	$vl 			= str_replace("/","-", $_POST['vlicencia']);
	$vlicencia 		= date('Y-m-d',strtotime($vl));
	$cl 			= str_replace("/","-", $_POST['cumpleanios']);
	$cumpleanios 	= date('Y-m-d',strtotime($cl));

	$sql = "UPDATE ".MAIN_DB_PREFIX."repartos_conductores 
			SET status='".$status."', idconductor='".$idc."', nombre='".$nombre."', direccion='".$direc."', 
				telefono='".$tel."', email='".$email."', depto='".$depto."', 
				puesto='".$puesto."', ingreso='".$ingreso."', vlicencia='".$vlicencia."', cumpleanios='".$cumpleanios."'
			WHERE entity=".$conf->entity." AND rowid=".$idcon;
	
	if( $rs = $db->query($sql) ){
		print "<script>window.location.href='nuevo.php?id=".$idcon."';</script>";
	}
}

if( $action == '' && $idcon == '' ) {
	//print_fiche_titre('Nuevo Conductor','','setup');
	$linkback="";
    print load_fiche_titre("Nuevo conductor",$linkback,'title_companies.png');

	print "<form enctype='multipart/form-data' method='post' action='nuevo.php' >";
	print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
	dol_fiche_head(null, 'card', '', 0, '');
	print "<table class='border' width='100%'>";
	print "<input type='hidden' name='action' value='add'>";
		print "<tr>";
			print "<td width='30%'>Estatus</td>";
			print "<td><select name='status'  >
					<option value='0'> </option>
					<option value='1'>Activo</option>
					<option value='2'>Inactivo</option></select></td>";
		print "</tr>";
		print "<tr>";
			print "<td>ID</td>";
			print "<td><input type='text' name='idc' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Nombre</td>";
			print "<td><input type='text' name='nombre' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Dirección</td>";
			print "<td><input type='text' name='direc' size='30' maxlength='255'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Teléfono</td>";
			print "<td><input type='text' name='tel' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Email</td>";
			print "<td><input type='text' name='email'  size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Departamento</td>";
			print "<td><input type='text' name='depto' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Puesto</td>";
			print "<td><input type='text' name='puesto' size='30'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Ingreso</td>";
			print "<td>";
				$form->select_date($dateop,'ingreso',0,0,0,'transaction');
			print "</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Vence licencia</td><td>";
				$form->select_date($dateop,'vlicencia',0,0,0,'transaction');
		print "</td></tr>";
		print "<tr>";
			print "<td>Cumplea&ntilde;os</td><td>";
				$form->select_date($dateop,'cumpleanios',0,0,0,'transaction');
		print "</td></tr>";
		//print "<tr>";
			//print "<td colspan='2' align='center'><input type='submit' name='add' value='Crear'></td>";
		//print "</tr>";
	print "</table>";

	dol_fiche_end();

	print '<div class="center">';
	print '<input type="submit" class="button" name="add" value="Crear conductor">';
	if ($backtopage) {
	    print ' &nbsp; ';
	    //print '<input type="submit" class="button" name="cancel" value="'.$langs->trans('Cancel').'">';
	}
	print '</div>'."\n";
	print '</form>'."\n";
}

if( $action == '' && $idcon > 0 ) {

	//print_fiche_titre('Conductor','','setup');
	$linkback="";
	print load_fiche_titre("Conductor",$linkback,'title_companies.png');

	$sql = "SELECT status, idconductor, nombre, direccion, telefono, email, 
				depto, puesto, ingreso, vlicencia, cumpleanios
			FROM ".MAIN_DB_PREFIX."repartos_conductores
			WHERE entity=".$conf->entity." AND rowid=".$idcon;
	$rs = $db->query($sql);
	$rq = $db->fetch_object($rs);

	dol_fiche_head(null, 'card', '', 0, '');

	print "<table class='border' width='100%'>";
		print "<tr>";
			print "<td width='30%'>Estatus</td>";
			$status="";
			if($rq->status==1){$status="Activo";}
			if($rq->status==2){$status="Baja";}
			print "<td>".$status."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>ID</td>";
			print "<td>".$rq->idconductor."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Nombre</td>";
			print "<td>".$rq->nombre."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Dirección</td>";
			print "<td>".$rq->direccion."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Teléfono</td>";
			print "<td>".$rq->telefono."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Email</td>";
			print "<td>".$rq->email."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Departamento</td>";
			print "<td>".$rq->depto."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Puesto</td>";
			print "<td>".$rq->puesto."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Ingreso</td>";
			print "<td>".date('d-m-Y',strtotime($rq->ingreso))."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Vence licencia</td>";
			print "<td>".date('d-m-Y',strtotime($rq->vlicencia))."</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Cumplea&ntilde;os</td>";
			print "<td>".date('d-m-Y',strtotime($rq->cumpleanios))."</td>";
		print "</tr>";
	print "</table>";

	dol_fiche_end();

	//print '<div class="tabsAction">'."\n";
	//print '<div class="inline-block divButAction"><a class="butAction" href="nuevo.php?id='.$idcon.'&action=edit">Modificar</a></div>';
	//print "</div>";

	print '<div class="tabsAction">'."\n";
    print '<div class="inline-block divButAction"><a class="butAction" href="nuevo.php?id='.$idcon.'&action=edit">'.$langs->trans("Modify").'</a></div>'."\n";
   // print '<div class="inline-block divButAction"><span id="action-delete" class="butActionDelete">'.$langs->trans('Delete').'</span></div>'."\n";

    print '<div class="inline-block divButAction"><a class="butActionDelete" href="nuevo.php?id='.$idcon.'&action=delete">'.$langs->trans('Delete').'</a></div>'."\n";
    if ($backtopage)
    {
        print ' &nbsp; ';
        //print '<input type="submit" class="button" name="cancel" value="'.$langs->trans('Cancel').'">';
    }
    print '</div>'."\n";
}

if( $action == 'edit' && $idcon>0 ) {

	$sql = "SELECT status, idconductor, nombre, direccion, telefono, email,
				depto, puesto, ingreso, vlicencia, cumpleanios
			FROM ".MAIN_DB_PREFIX."repartos_conductores
			WHERE entity=".$conf->entity." AND rowid=".$idcon;
	$rs = $db->query($sql);
	$rq = $db->fetch_object($rs);

	//print_fiche_titre('Conductor','','setup');
	$linkback="";
	print load_fiche_titre("Conductor",$linkback,'title_companies.png');

	print "<form method='POSt' action='nuevo.php?id=".$idcon."'>";
	
	dol_fiche_head(null, 'card', '', 0, '');

	print "<table class='border' width='100%'>";
		print "<input type='hidden' name='action' value='confedit'>";
		print "<tr>";
		print "<td width='30%'>Estatus</td>";
		$a='';$b='';$c='';
		if($rq->status==0){$a=' SELECTED';}
		if($rq->status==1){$b=' SELECTED';}
		if($rq->status==2){$c=' SELECTED';}
		print "<td><select name='status'  >
					<option value='0' ".$a."> </option>
					<option value='1' ".$b.">Activo</option>
					<option value='2' ".$c.">Inactivo</option></select></td>";
		print "</tr>";
		print "<tr>";
			print "<td>ID</td>";
			print "<td><input type='text' name='idc' size='30' value='".$rq->idconductor."'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Nombre</td>";
			print "<td><input type='text' name='nombre' size='30'  value='".$rq->nombre."'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Dirección</td>";
			print "<td><input type='text' name='direc' size='30' maxlength='255' value='".$rq->direccion."'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Teléfono</td>";
			print "<td><input type='text' name='tel' size='30' value='".$rq->telefono."'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Email</td>";
			print "<td><input type='text' name='email'  size='30' value='".$rq->email."'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Departamento</td>";
			print "<td><input type='text' name='depto' size='30' value='".$rq->depto."'></td>";
		print "</tr>";
		print "<tr>";
			print "<td>Puesto</td>";
			print "<td><input type='text' name='puesto' size='30' value='".$rq->puesto."'></td>";
		print "</tr>";
		print "<tr>";
		print "<td>Ingreso</td>";
			print "<td>";
			$ig=$rq->ingreso;
			$form->select_date(strtotime($ig),'ingreso',0,0,0,'transaction');
			print "</td>";
		print "</tr>";
		print "<tr>";
			print "<td>Vence licencia</td><td>";
			$vl=$rq->vlicencia;
			$form->select_date(strtotime($vl),'vlicencia',0,0,0,'transaction');
			print "</td></tr>";
		print "<tr>";
			print "<td>Cumplea&ntilde;os</td><td>";
			$cp=$rq->cumpleanios;
			$form->select_date(strtotime($cp),'cumpleanios',0,0,0,'transaction');
			print "</td></tr>";
		//print "<tr>";
			//print "<td colspan='2' align='center'><input type='submit' name='edit' value='Actualizar'></td>";
		//print "</tr>";
	print "</table>";

	dol_fiche_end();

	print '<div class="center">';
	print '<input type="submit" class="button" name="edit" value="'.$langs->trans("Save").'">';
	print '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    print '<input type="submit" class="button" name="cancel" value="'.$langs->trans("Cancel").'">';
	if ($backtopage) {
	    print ' &nbsp; ';
	    //print '<input type="submit" class="button" name="cancel" value="'.$langs->trans('Cancel').'">';
	}
	print '</div>'."\n";
	print '</form>'."\n";
	
}

if ($action == 'delete') {
		
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $idcon, $langs->trans('Eliminar conductor'), $langs->trans('Esta seguro de eliminar este registro'), 'confirm_delete', '', 0, 1);
		print $formconfirm;
}


// Action to delete
if ($action == 'confirm_delete')
{

	
		$sql = "DELETE FROM ".MAIN_DB_PREFIX."repartos_conductores 
				WHERE entity=".$conf->entity." AND rowid=".$idcon;
		

		if( $rs = $db->query($sql) ){
			
			print "<script>window.location.href='list.php?id=".$idcon."';</script>";
		}
		
}

