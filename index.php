<?php
error_reporting(-1);
$res=@include("../main.inc.php");                                // For root directory
if (! $res) $res=include("../../main.inc.php");  

llxHeader("","Repartos",'');

print_fiche_titre('Repartos','','setup');

print '<div class="fichecenter">';

print '<div class="fichethirdleft">';

// Alerta de facturas que desde dolibarr se le puso status de pendiente por entregar y que no se haya generado orden de trafico
print '<table class="noborder nohover centpercent">';
    print '<tr class="liste_titre">';
        print '<td align="center" colspan="3">Facturas pendientes de envio.</td>';
    print '</tr>';
    $sql = "
    SELECT
		a.rowid,
		a.facnumber,
		a.fk_statut,
		d.canprod
	FROM
		llx_facture a,
		(
			SELECT
				count(c.rowid) AS canprod,
				c.fk_facture
			FROM
				llx_facturedet c
			WHERE
				fk_product > 0
			AND product_type = 0
			GROUP BY
				fk_facture
		) AS d
	WHERE
		a.entity = 1
	AND (
		fk_statut != 0
		AND fk_statut != 3
	)
	AND a.rowid NOT IN (
		SELECT
			b.fk_facture
		FROM
			llx_repartos_gestion_facture b
		WHERE
			b.entity = 1
		AND b.completa = 1
	)
	AND a.rowid IN (
		SELECT
			b.fk_facture
		FROM
			llx_pos_facture_envio b
	)
	AND d.fk_facture = a.rowid
	ORDER BY
		a.facnumber
    ";
    $rq = $db->query($sql);
    while( $rs = $db->fetch_object($rq) ){
        print '<tr>';
            print '<td>'.$rs->facnumber.'</td>';
        print '</tr>';
    }
print '</table>';

print '</div>';

print '<div class="fichetwothirdright">';
    print '<div class="ficheaddleft">';

//Informacion de cuantas facturas y dinero estan pendientes a pago por cliente

print '<table class="noborder nohover centpercent">';
    print '<tr class="liste_titre">';
        print '<td align="center" colspan="3">Facturas pendientes de cobro.</td>';
    print '</tr>';
    $sql = "
    SELECT
        t2.name_alias AS alias,
        t2.nom AS client,
        SUM(t.total_ttc) AS cantidadT,
        COUNT(*) AS totalfact
    FROM
        llx_facture AS t
    INNER JOIN llx_societe AS t2 ON t2.rowid = t.fk_soc
    WHERE
        t.fk_statut = 1
    AND t.rowid IN (
		SELECT
			b.fk_facture
		FROM
			llx_pos_facture_envio b
	)
    AND t.paye = 0
    GROUP BY
        client
    ";
    $rq = $db->query($sql);
    while( $rs = $db->fetch_object($rq) ){
        print '<tr>';
            print '<td>'.$rs->client.'('.$rs->alias.')'.'</td>';
            print '<td>'.'Dinero total: '.$rs->cantidadT.'</td>';
            print '<td>'.'Número de facturas: '.$rs->totalfact.'</td>';
        print '</tr>';
    }
print '</table>';


    print '</div>';
print '</div>';

print '</div>'; // End for firts div

// Nueva linea con datos relevantes

print '<div class="fichecenter">';

print '<div class="fichethirdleft">';

// Ultimos 5 conductores agregados
print '<table class="noborder nohover centpercent">';
    print '<tr class="liste_titre">';
        print '<td align="center" colspan="3">Últimos 5 conductores agregados.</td>';
    print '</tr>';
    $sql = "
    SELECT
        CONCAT(a.idconductor, ' - ',a.nombre) AS ultimos
    FROM
        llx_repartos_conductores AS a
    ORDER BY
        a.rowid DESC
    LIMIT 5
    ";
    $rq = $db->query($sql);
    while( $rs = $db->fetch_object($rq) ){
        print '<tr>';
            print '<td>'.$rs->ultimos.'</td>';
        print '</tr>';
    }
print '</table>';


// Últimos 5 vehículos agregados

print '<table class="noborder nohover centpercent">';
    print '<tr class="liste_titre">';
        print '<td align="center" colspan="3">Últimos 5 vehículos agregados.</td>';
    print '</tr>';
    $sql = "
    SELECT
        CONCAT(a.idvehiculo, ' - ',a.auto) AS ultimos
    FROM
        llx_repartos_vehiculos AS a
    ORDER BY
        a.rowid DESC
    LIMIT 5
    ";
    $rq = $db->query($sql);
    while( $rs = $db->fetch_object($rq) ){
        print '<tr>';
            print '<td>'.$rs->ultimos.'</td>';
        print '</tr>';
    }
print '</table>';

// Estadísticas del modulo de repartos

print '<table class="noborder nohover centpercent">';
    print '<tr class="liste_titre">';
        print '<td align="center" colspan="3">Estadísticas.</td>';
    print '</tr>';
    // Total de tráficos con el estatus de borrador
    $sql = "
    SELECT
        COUNT(*) AS total
    FROM
        llx_repartos_gestion t,
        llx_repartos_vehiculos t2,
        llx_repartos_conductores t3
    WHERE
        t.fk_vehiculo = t2.rowid
    AND t.fk_conductor = t3.rowid
    AND t.status = 0
    ";
    $rq = $db->query($sql);
    $rs = $db->fetch_object($rq);
    print '<tr>';
        print '<td>'.'Total de tráficos con el estatus de borrador: '.$rs->total.'</td>';
    print '</tr>';
    // Total de tráficos con el estatus de validado
    $sql = "
    SELECT
        COUNT(*) AS total
    FROM
        llx_repartos_gestion t,
        llx_repartos_vehiculos t2,
        llx_repartos_conductores t3
    WHERE
        t.fk_vehiculo = t2.rowid
    AND t.fk_conductor = t3.rowid
    AND t.status = 1
    ";
    $rq = $db->query($sql);
    $rs = $db->fetch_object($rq);
    print '<tr>';
        print '<td>'.'Total de tráficos con el estatus de validado: '.$rs->total.'</td>';
    print '</tr>';
    // Total de tráficos con el estatus de entregado
    $sql = "
    SELECT
        COUNT(*) AS total
    FROM
        llx_repartos_gestion t,
        llx_repartos_vehiculos t2,
        llx_repartos_conductores t3
    WHERE
        t.fk_vehiculo = t2.rowid
    AND t.fk_conductor = t3.rowid
    AND t.status = 2
    ";
    $rq = $db->query($sql);
    $rs = $db->fetch_object($rq);
    print '<tr>';
        print '<td>'.'Total de tráficos con el estatus de entregado: '.$rs->total.'</td>';
    print '</tr>';
print '</table>';

print '</div>';